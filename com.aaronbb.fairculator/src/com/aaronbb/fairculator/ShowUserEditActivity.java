package com.aaronbb.fairculator;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.aaronbb.fairculator.daten.MeineDaten;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Log;

public class ShowUserEditActivity extends Activity {
	public MeineDaten benutzer, benutzer_editor;
	private SharedPreferences sharedprefs;
	private Menu menu;

	// Dialog IDs
	private static final int D_CHANGE_NAME = 1;
	private static final int D_CHANGE_EMAIL = 2;
	private static final int D_CHANGE_PASS = 3;
	private static final int D_USER_VERBINDEN = 4;

	// Formfelder
	private EditText e_name, e_email, e_pass_old, e_pass_new, e_pass_new_repeat;
	private LinearLayout e_pass_container;
	private boolean fehlergefunden = false;
	Boolean wizard_mode = false;
	private Map<Integer, String> d_liste_user_verbinden;
	private int d_liste_user_selected = -1;

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// new Loader().execute(true);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Formfelder initialisieren
		e_name = new EditText(this);
		e_email = new EditText(this);
		e_pass_old = new EditText(this);
		e_pass_new = new EditText(this);
		e_pass_new_repeat = new EditText(this);
		e_pass_container = new LinearLayout(this);

		// Benutzeremail und Passwort laden
		sharedprefs = getSharedPreferences("fairculator", MODE_PRIVATE);
		benutzer = new MeineDaten(sharedprefs.getString("email", null), sharedprefs.getString("pass", null));

		// Show the Up button in the action bar.
		setupActionBar();

	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		new Loader().execute(false);
	}

	@Override
	@Deprecated
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		switch (id) {
			case D_CHANGE_NAME:
				e_name.setSelectAllOnFocus(true);
				e_name.setHint(R.string.hint_name);
				final AlertDialog d_name = new AlertDialog.Builder(this).setTitle(R.string.hint_name).setView(e_name)
						.setPositiveButton(R.string.d_ok, null).setNegativeButton(R.string.d_abbrechen, null).create();
				d_name.setOnShowListener(new DialogInterface.OnShowListener() {
					@Override
					public void onShow(DialogInterface dialog) {
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.showSoftInput(e_name, InputMethodManager.SHOW_IMPLICIT);
						Button b = d_name.getButton(AlertDialog.BUTTON_POSITIVE);
						b.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View view) {
								if (e_name.getText().toString().length() == 0) {
									showError(e_name, getString(R.string.e_bitteangeben));
								} else {
									benutzer_editor.setName(e_name.getText().toString());
									((TextView) findViewById(R.id.show_user_edit_name)).setText(e_name.getText()
											.toString());
									d_name.dismiss();
									if (wizard_mode)
										showDialog(D_CHANGE_EMAIL);
								}
							}
						});
					}
				});
				return d_name;
			case D_CHANGE_EMAIL:
				e_email.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
				e_email.setHint(R.string.hint_email);
				e_email.setSelectAllOnFocus(true);
				final AlertDialog d_email = new AlertDialog.Builder(this).setTitle(R.string.hint_email).setView(e_email)
						.setPositiveButton(R.string.d_ok, null).setNegativeButton(R.string.d_abbrechen, null).create();
				d_email.setOnShowListener(new DialogInterface.OnShowListener() {
					@Override
					public void onShow(DialogInterface dialog) {
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.showSoftInput(e_email, InputMethodManager.SHOW_IMPLICIT);
						Button b = d_email.getButton(AlertDialog.BUTTON_POSITIVE);
						b.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View view) {
								if (!android.util.Patterns.EMAIL_ADDRESS.matcher(e_email.getText().toString())
										.matches()) {
									showError(e_email, getString(R.string.e_keineemailadresse));
								} else {
									benutzer_editor.setEmail(e_email.getText().toString());
									((TextView) findViewById(R.id.show_user_edit_email)).setText(e_email.getText()
											.toString());
									d_email.dismiss();
									if (wizard_mode)
										showDialog(D_CHANGE_PASS);
								}
							}
						});
					}
				});
				return d_email;
			case D_CHANGE_PASS:
				e_pass_container.setOrientation(LinearLayout.VERTICAL);
				LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

				e_pass_old.setHint(R.string.hint_altespasswort);
				e_pass_old.setSelectAllOnFocus(true);
				e_pass_old.requestFocus();
				e_pass_old.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
				e_pass_old.setTransformationMethod(PasswordTransformationMethod.getInstance());
				e_pass_container.addView(e_pass_old, lp);

				e_pass_new.setHint(R.string.hint_neuespasswort);
				e_pass_new.setSelectAllOnFocus(true);
				e_pass_new.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
				e_pass_new.setTransformationMethod(PasswordTransformationMethod.getInstance());
				e_pass_container.addView(e_pass_new, lp);

				e_pass_new_repeat.setHint(R.string.hint_neuespasswortwiederholen);
				e_pass_new_repeat.setSelectAllOnFocus(true);
				e_pass_new_repeat.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
				e_pass_new_repeat.setTransformationMethod(PasswordTransformationMethod.getInstance());
				e_pass_container.addView(e_pass_new_repeat, lp);

				final AlertDialog d_pass = new AlertDialog.Builder(this).setTitle(R.string.text_passwort)
						.setView(e_pass_container).setPositiveButton(R.string.d_ok, null)
						.setNegativeButton(R.string.d_abbrechen, null).create();
				d_pass.setOnShowListener(new DialogInterface.OnShowListener() {
					@Override
					public void onShow(DialogInterface dialog) {
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.showSoftInput(e_pass_old, InputMethodManager.SHOW_IMPLICIT);
						Button b = d_pass.getButton(AlertDialog.BUTTON_POSITIVE);
						b.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View view) {
								Boolean fehlergefunden = false;
								if (e_pass_old.getVisibility() == View.VISIBLE
										&& !e_pass_old.getText().toString().equals(benutzer.getPasswort())) {
									showError(e_pass_old, getString(R.string.e_passwortfalsch));
									fehlergefunden = true;
								}
								if (e_pass_new.getText().toString().length() == 0) {
									showError(e_pass_new, getString(R.string.e_bitteangeben));
									fehlergefunden = true;
								}
								if (!e_pass_new.getText().toString().equals(e_pass_new_repeat.getText().toString())) {
									showError(e_pass_new_repeat, getString(R.string.e_stimmtnichtueberein));
									fehlergefunden = true;
								}
								if (!fehlergefunden) {
									benutzer_editor.setPasswort(e_pass_new.getText().toString());
									d_pass.dismiss();
								}
							}
						});
					}
				});
				return d_pass;

			case D_USER_VERBINDEN:
				AlertDialog.Builder d_user_verbinden = new AlertDialog.Builder(this);
				d_user_verbinden.setTitle(R.string.d_benutzerverknuepfen);

				if (d_liste_user_verbinden != null && d_liste_user_verbinden.size() > 0) {
					d_user_verbinden.setItems(
							d_liste_user_verbinden.values().toArray(new CharSequence[d_liste_user_verbinden.size()]),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									int i = 0;
									for (int k : d_liste_user_verbinden.keySet()) {
										if (i == which) {
											d_liste_user_selected = k;
										}
										i++;
									}
									d_liste_user_verbinden = null;
									new Loader().execute(true);
								}
							}).setPositiveButton(R.string.d_nichtverknuepfen, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							d_liste_user_selected = 0;
							d_liste_user_verbinden = null;
							new Loader().execute(true);
						}
					});
				}
				return d_user_verbinden.create();
		}
		return super.onCreateDialog(id);
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.show_user_edit, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				// This ID represents the Home or Up button. In the case of this
				// activity, the Up button is shown. Use NavUtils to allow users
				// to navigate up one level in the application structure. For
				// more details, see the Navigation pattern on Android Design:
				//
				// http://developer.android.com/design/patterns/navigation.html#up-vs-back
				//
				NavUtils.navigateUpFromSameTask(this);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void speichern() {
		// Wenn sich die Email ge�ndert hat und es kein neuer Benutzer ist, denn
		// dann wird ein Text entspr. angezeigt
		if (benutzer.getEmail() != null && !benutzer_editor.getEmail().equalsIgnoreCase(benutzer.getEmail()))
			Toast.makeText(getBaseContext(), getString(R.string.t_emailbestaetigungkommt), Toast.LENGTH_LONG).show();

		// Wenn sich das Passwort ge�ndert hat
		if (benutzer_editor.getPasswort() != null) {
			SharedPreferences.Editor prefEditor = sharedprefs.edit();
			prefEditor.putString("pass", benutzer_editor.getPasswort());
			prefEditor.commit();
		}
		new Loader().execute(true);
	}

	private boolean showError(EditText ETreferenz, String fehlertext) {
		Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
		ETreferenz.startAnimation(shake);
		ETreferenz.setError(fehlertext);
		ETreferenz.requestFocus();
		return true;
	}

	public class Loader extends AsyncTask<Boolean, String, Boolean> {
		ProgressDialog progDailog;
		private Boolean neuerBenutzerGespeichert = false;
		private Boolean alterBenutzerGespeichert = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDailog = new ProgressDialog(ShowUserEditActivity.this);
			progDailog.setMessage(getString(R.string.d_laden));
			progDailog.setIndeterminate(false);
			progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDailog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					Loader.this.cancel(true);
					finish();
				}
			});
			progDailog.show();
		}

		protected Boolean doInBackground(Boolean... auchspeichern) {

			// HTTP Client that supports streaming uploads and downloads
			HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, "UTF-8");

			DefaultHttpClient httpclient = new DefaultHttpClient(params);

			List<NameValuePair> httppostpairs = new ArrayList<NameValuePair>();

			// Wenn gespeichert werden soll
			if (auchspeichern[0]) {
				httppostpairs.add(new BasicNameValuePair("u_name", benutzer_editor.getName()));
				httppostpairs.add(new BasicNameValuePair("u_email", benutzer_editor.getEmail()));
				httppostpairs.add(new BasicNameValuePair("u_pass", benutzer_editor.getPasswort()));

				if (d_liste_user_selected >= 0)
					httppostpairs.add(new BasicNameValuePair("u_kind_id", String.valueOf(d_liste_user_selected)));
			}

			// Define that I want to use the POST method to grab data from
			// the provided URL
			HttpPost httppost = new HttpPost(StartActivity.DB_CONNECTION_URL + "?action=loadownuser");

			httppostpairs.add(new BasicNameValuePair("email", benutzer.getEmail()));
			httppostpairs.add(new BasicNameValuePair("pass", benutzer.getPasswort()));

			try {
				httppost.setEntity(new UrlEncodedFormEntity(httppostpairs));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Web service used is defined
			// httppost.setHeader("Content-type", "application/json");
			httppost.setHeader("Content-type", "application/x-www-form-urlencoded");

			// Used to read data from the URL
			InputStream inputStream = null;

			// Will hold the whole all the data gathered from the URL
			String result = null;

			try {

				// Get a response if any from the web service
				HttpResponse response = httpclient.execute(httppost);

				// The content from the requested URL along with headers, etc.
				HttpEntity entity = response.getEntity();

				// Get the main content from the URL
				inputStream = entity.getContent();

				// JSON is UTF-8 by default
				// BufferedReader reads data from the InputStream until the
				// Buffer is full
				BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);

				// Will store the data
				StringBuilder theStringBuilder = new StringBuilder();

				String line = null;

				// Read in the data from the Buffer untilnothing is left
				while ((line = reader.readLine()) != null) {

					// Add data from the buffer to the StringBuilder
					theStringBuilder.append(line + "\n");
				}

				// Store the complete data in result
				result = theStringBuilder.toString();

			} catch (Exception e) {
				e.printStackTrace();
				return false;
			} finally {

				// Close the InputStream when you're done with it
				try {
					if (inputStream != null)
						inputStream.close();
				} catch (Exception e) {
				}
			}

			JSONObject jsonObject;
			try {
				// Get the root JSONObject
				jsonObject = new JSONObject(result);

				if (!jsonObject.getJSONArray("userdata").isNull(0)) {
					// Benutzer laden
					JSONObject jsonuser = jsonObject.getJSONArray("userdata").getJSONObject(0);
					benutzer.setName(jsonuser.getString("name"));
					benutzer.setId(jsonuser.getInt("id"));

				}

				d_liste_user_verbinden = new LinkedHashMap<Integer, String>();
				if (jsonObject.has("error_userverbinden")) {
					// Wenn ein Benutzer zum verkn�fen vorhanden ist
					JSONArray j_user_verbinden = jsonObject.getJSONArray("error_userverbinden");
					for (int i = 0; i < j_user_verbinden.length(); i++)
						d_liste_user_verbinden.put(j_user_verbinden.getJSONObject(i).getInt("id"), j_user_verbinden
								.getJSONObject(i).getString("name")
								+ " ("
								+ getString(R.string.text_erstelltvon)
								+ " "
								+ j_user_verbinden.getJSONObject(i).getString("owner_name") + ")");
				} else {

					if (!jsonObject.getJSONArray("ownuserdata").isNull(0)) {
						// Wenn ein neuer Benutzer hinzugef�gt wurde Flag setzen
						// damit in onPostExecute darauf eingegangen werden kann
						if (auchspeichern[0] && benutzer_editor.getId() == 0)
							neuerBenutzerGespeichert = true;

						// Wenn ein existierender Benutzer hinzugef�gt wurde
						// Flag
						// setzen damit in onPostExecute darauf eingegangen
						// werden
						// kann
						if (auchspeichern[0] && benutzer_editor.getId() != 0)
							alterBenutzerGespeichert = true;

						// Benutzer laden
						JSONObject jsonuser = jsonObject.getJSONArray("ownuserdata").getJSONObject(0);
						benutzer_editor = new MeineDaten(jsonuser);

					} else {
						// Wenn ein neuer Benutzer angelegt wird
						benutzer_editor = new MeineDaten();
					}
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return true;

		}

		protected void onPostExecute(Boolean result) {
			if (progDailog.isShowing()) {
				progDailog.dismiss();
			}

			if (!result) {
				startActivity((new Intent(getBaseContext(), StartActivity.class))
						.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				return;
			}
			if (d_liste_user_verbinden.size() > 0) {
				showDialog(D_USER_VERBINDEN);
				wizard_mode = false;
				return;
			} else {
				if (neuerBenutzerGespeichert) {
					TextView t = new TextView(ShowUserEditActivity.this);
					t.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
					t.setText(R.string.text_anmeldungbestlink);
					t.setGravity(Gravity.CENTER);
					setContentView(t);
					return;
				}
			}

			setContentView(R.layout.activity_show_user_edit);

			if (benutzer_editor.getId() == 0) {
				wizard_mode = true;
				e_pass_old.setVisibility(View.GONE);
				showDialog(D_CHANGE_NAME);
			} else {
				wizard_mode = false;
				e_pass_old.setVisibility(View.VISIBLE);
			}

			benutzer.setSync(true);
			try {
				menu.findItem(R.id.show_event_abmelden).setVisible(true);
			} catch (NullPointerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// onclicklistener
			findViewById(R.id.show_user_edit_name_field).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					showDialog(D_CHANGE_NAME);
				}
			});
			findViewById(R.id.show_user_edit_email_field).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					showDialog(D_CHANGE_EMAIL);
				}
			});
			findViewById(R.id.show_user_edit_pass_field).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					showDialog(D_CHANGE_PASS);
				}
			});
			findViewById(R.id.show_user_edit_save).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					speichern();
				}
			});

			// Anzeigen und Formuarfelder belegen
			e_name.setText(benutzer_editor.getName());
			((TextView) findViewById(R.id.show_user_edit_name)).setText(benutzer_editor.getName());

			e_email.setText(benutzer_editor.getEmail());
			((TextView) findViewById(R.id.show_user_edit_email)).setText(benutzer_editor.getEmail());

			e_pass_old.setText("");
			e_pass_new.setText("");
			e_pass_new_repeat.setText("");
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.
	}
}
