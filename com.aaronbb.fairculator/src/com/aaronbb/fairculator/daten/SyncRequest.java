package com.aaronbb.fairculator.daten;

import java.util.HashMap;

public interface SyncRequest {
	
	public String getQuery();
	public HashMap<String, String> getPostVars();
	public boolean isSync();
	public void setSync(boolean s);
	public void setId(int id);
	public int getId();

}
