package com.aaronbb.fairculator.daten;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.json.JSONObject;

import android.R.bool;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.aaronbb.fairculator.StartActivity;

public class Bill implements Serializable, SyncRequest {
	/**
	 * 
	 */
	private static final long serialVersionUID = -190104812446381016L;
	private int id = 0;
	private int event_id;
	private int owner_id;
	private Map<Integer, Eigenanteil> userinkleigenanteile = new HashMap<Integer, Eigenanteil>();
	private Double gemeinsamer_teil = null;

	private Double total_betrag;
	private String shop;
	private Date datum;
	private String text;
	private String foto;
	private String foto_checksum;
	private boolean loaded, saved, isSync = false;

	// zuletzt augef�hrte HttpAnfrage f�r diesen Bill
	private HttpAnfrage recentHttpAnfrage = null;

	public Bill(int id, int event_id, int owner_id, Double total_betrag, String shop, Date datum, String text) {
		this.id = id;
		this.event_id = event_id;
		this.owner_id = owner_id;
		this.total_betrag = total_betrag;
		this.shop = shop;
		this.datum = datum;
		this.text = text;
		this.foto = null;
		this.foto_checksum = "";
		this.saved = false;
	}

	public Bill(JSONObject JSONbill, Event event) {
		try {
			setId(JSONbill.getInt("id"));
			setEvent_id((Integer) JSONbill.getInt("event"));
			setOwner_id((Integer) JSONbill.getInt("owner"));
			setTotal_betrag(Double.parseDouble(JSONbill.getString("betrag")));
			setShop(JSONbill.getString("shop"));
			setDatum(new SimpleDateFormat("yyyy-MM-dd").parse(JSONbill.getString("datum")));
			setText(JSONbill.getString("text"));
			setSaved(false);

			String[] user_id = JSONbill.getString("users").split(",");
			String[] betrag_aufteilung = JSONbill.getString("betrag_aufteilung").split(",");
			String[] pfand_aufteilung = JSONbill.getString("pfand_aufteilung").split(",");

			for (int j = 0; j < user_id.length; j++) {
				putUserInklEigenanteil(
						event.getCouser(Integer.valueOf(user_id[j])),
						betrag_aufteilung.length > j && betrag_aufteilung[j] != "" ? Double
								.parseDouble(betrag_aufteilung[j]) : 0.0,
						pfand_aufteilung.length > j && pfand_aufteilung[j] != "" ? Double
								.parseDouble(pfand_aufteilung[j]) : 0.0);
			}

			if (JSONbill.has("md5(`bild`)")) {
				setFotoChecksum((JSONbill.getString("md5(`bild`)") == "null") ? null : JSONbill
						.getString("md5(`bild`)"));
			}
			setFoto(null);
			
			//Wenn Bill durch JSON erzeugt dann m�sste er synchron sein
			setSync(true);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Bill(MeineDaten ich, Event event, int bill_id) {
		setId(bill_id);
		setEvent_id(event.getId());
		setOwner_id(ich.getId());
		setTotal_betrag(0.0);
		setShop("");
		setDatum(new Date());
		setText("");
		setFotoChecksum("");
		setFoto(null);
		setSaved(false);
		setSync(true); //da sonst der leere Bill gespeichert werden w�rde
		for (Entry<Integer, Benutzer> cu : event.getCouserMap().entrySet()) {
			putUserInklEigenanteil(cu.getValue(), 0.0, 0.0);
		}
	}

	public void update(Bill bill) {
		this.setId(bill.getId());
		this.setEvent_id(bill.getEvent_id());
		this.setOwner_id(bill.getOwner_id());
		this.setUserInklEigenanteile(bill.getUserInklEigenanteile());
		this.setGemeinsamer_teil(bill.getGemeinsamer_teil());
		this.setTotal_betrag(bill.getTotal_betrag());
		this.setShop(bill.getShop());
		this.setDatum(bill.getDatum());
		this.setText(bill.getText());
		this.setFotoChecksum(bill.getFotoChecksum());
		this.setFoto(bill.getFoto());
		this.setSaved(bill.isSaved());
		this.setSync(bill.isSync());
		this.setRecentHttpAnfrage(bill.getRecentHttpAnfrage());
	}

	public int getEvent_id() {
		return event_id;
	}

	public void setEvent_id(int event_id) {
		this.event_id = event_id;
	}

	public int getOwner_id() {
		return owner_id;
	}

	public void setOwner_id(int owner_id) {
		this.owner_id = owner_id;
	}

	public Double getEigenanteilById(int user_id) {
		if (!userinkleigenanteile.isEmpty() && userinkleigenanteile.get(user_id) != null
				&& userinkleigenanteile.get(user_id).getBetrag() != null) {
			return userinkleigenanteile.get(user_id).getBetrag();
		} else
			return null;
	}

	public Double getEigenPfandanteilById(int user_id) {
		if (!userinkleigenanteile.isEmpty() && userinkleigenanteile.get(user_id) != null
				&& userinkleigenanteile.get(user_id).getPfand() != null) {
			return userinkleigenanteile.get(user_id).getPfand();
		} else
			return 0.0;
	}

	public Boolean isAmEinkaufBeteiling(int user_id) {
		if (userinkleigenanteile.get(user_id) != null) {
			return true;
		} else
			return false;
	}

	public Benutzer getCouser(int id) {
		return userinkleigenanteile.get(id).getCouser();
	}

	public ArrayList<Integer> getCouserIds() {
		return new ArrayList(userinkleigenanteile.keySet());
	}

	public void putUserInklEigenanteil(Benutzer couser, Double eigen_betrag, Double pfand_betrag) {
		if (couser.getId() != 0) {
			this.userinkleigenanteile.put(couser.getId(), new Eigenanteil(couser, eigen_betrag, pfand_betrag));
			gemeinsamer_teil = null;
		}
	}

	// public Boolean putUserInklFromString(int user_id, String eigen_betrag) {
	// Double eb = StartActivity.parseMoney(eigen_betrag);
	// if(eb==null) return false;
	// this.userinkleigenanteile.put(user_id, new Eigenanteil(eb,
	// this.userinkleigenanteile.get(user_id).getPfand()));
	// gemeinsamer_teil = null;
	// return true;
	// }

	public void delUserInklEigenanteil(int user_id) {
		this.userinkleigenanteile.remove(user_id);
		gemeinsamer_teil = null;
	}

	public int sizeOfUserInklEigenanteile() {
		return userinkleigenanteile.size();
	}

	public ArrayList<Integer> getUserKeysByPosition() {
		ArrayList<Integer> a = new ArrayList<Integer>(userinkleigenanteile.keySet());
		Collections.sort(a);
		return a;
	}

	public Map<Integer, Eigenanteil> getUserInklEigenanteile() {
		return userinkleigenanteile;
	}

	public void setUserInklEigenanteile(Map<Integer, Eigenanteil> b) {
		this.userinkleigenanteile = b;
		gemeinsamer_teil = null;
	}

	public Double getGesamterWertderRechnung() {
		Double summe = getTotal_betrag();
		for (Eigenanteil e : userinkleigenanteile.values()) {
			summe = summe + e.getPfand();
		}
		return summe;
	}

	public Double getTotal_betrag() {
		return total_betrag;
	}

	public void setTotal_betrag(Double total_betrag) {
		this.total_betrag = total_betrag;
		gemeinsamer_teil = null;
	}

	public Boolean setTotal_betrag(String total_betrag, Locale l) {
		Double tb = StartActivity.parseMoney(total_betrag, l);
		if (total_betrag.equals(""))
			tb = 0.0;
		if (tb == null)
			return false;
		this.total_betrag = tb;
		gemeinsamer_teil = null;
		return true;
	}

	public Double getGemBetrag() {

		if (gemeinsamer_teil == null) {
			Double summe = getTotal_betrag();
			for (Entry<Integer, Eigenanteil> e : userinkleigenanteile.entrySet()) {
				summe = summe - e.getValue().getBetrag() + e.getValue().getPfand();
			}
			return gemeinsamer_teil = summe;
		} else
			return gemeinsamer_teil;

	}

	public Double getAnteilbyId(int id) {
		if (userinkleigenanteile.size() > 0 && userinkleigenanteile.get(id) != null
				&& userinkleigenanteile.get(id).getBetrag() != null) {
			return userinkleigenanteile.get(id).getBetrag() - userinkleigenanteile.get(id).getPfand()
					+ (getGemBetrag() / userinkleigenanteile.size());
		} else
			return 0.0;
	}

	public String getShop() {
		return shop;
	}

	public Boolean setShop(String shop) {
		if (shop.length() != 0) {
			this.shop = shop;
			return true;
		} else
			return false;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public Boolean setDatum(String datum) {
		try {
			this.datum = (Date) StartActivity.dateFormat.parseObject(datum);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isLoaded() {
		return loaded;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

	public Double getGuthabenAnBill(int userid) {
		int besitzerdesbills = this.getOwner_id();

		if (besitzerdesbills == userid) {
			Double anteile = 0.0;
			// Wenn man selbst den Bill bezahlt hat

			// Alle Eigenanteile durchgehen
			for (int a : this.userinkleigenanteile.keySet()) {
				if (a != userid)
					anteile += this.getAnteilbyId(a);
			}
			return anteile;

		} else {
			// Bei Bills die andere bezahlt haben
			return -this.getAnteilbyId(userid);
		}
	}

	public boolean isSaved() {
		return saved;
	}

	public void setSaved(boolean saved) {
		this.saved = saved;
	}

	public String getFoto() {
		return this.foto;
	}

	public Bitmap getFotoBitmap() {
		byte[] decodedString = Base64.decode(this.foto, 0);
		return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getFotoChecksum() {
		return foto_checksum;
	}

	public void setFotoChecksum(String foto_checksum) {
		this.foto_checksum = (foto_checksum != null && foto_checksum.length() == 0) ? null : foto_checksum;
	}

	public class Eigenanteil implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -4109090187396580733L;
		private Benutzer couser;
		private Double betrag, pfand = 0.0;

		public Eigenanteil(Benutzer couser, Double betrag, Double pfand) {
			this.setCouser(couser);
			this.setBetrag(betrag);
			this.setPfand(pfand);
		}

		public Double getBetrag() {
			return betrag;
		}

		public void setBetrag(Double betrag) {
			this.betrag = betrag;
		}

		public Double getPfand() {
			return pfand;
		}

		public void setPfand(Double pfand) {
			this.pfand = pfand;
		}

		public Benutzer getCouser() {
			return couser;
		}

		public void setCouser(Benutzer couser) {
			this.couser = couser;
		}

	}

	public int getCouserSize() {
		return userinkleigenanteile.size();
	}

	public HttpAnfrage getRecentHttpAnfrage() {
		return recentHttpAnfrage;
	}

	public void overwriteRecentHttpAnfrage(HttpAnfrage recentHttpAnfrage) {
		if (this.recentHttpAnfrage != null)
			this.recentHttpAnfrage.kill();
		this.recentHttpAnfrage = recentHttpAnfrage;
	}

	public Double getGemeinsamer_teil() {
		return gemeinsamer_teil;
	}

	public void setGemeinsamer_teil(Double gemeinsamer_teil) {
		this.gemeinsamer_teil = gemeinsamer_teil;
	}

	public void setRecentHttpAnfrage(HttpAnfrage recentHttpAnfrage) {
		this.recentHttpAnfrage = recentHttpAnfrage;
	}

	@Override
	public String getQuery() {
		return "action=loadbill&bill_id=" + this.getId() + "&event_id=" + this.getEvent_id();
	}

	@Override
	public HashMap<String, String> getPostVars() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		HashMap<String, String> pairs = new HashMap<String, String>();
		pairs.put("b_id", String.valueOf(this.getId() > 0 ? this.getId() : 0));
		pairs.put("b_event", String.valueOf(this.getEvent_id()));
		pairs.put("b_owner", String.valueOf(this.getOwner_id()));
		pairs.put("b_betrag", String.valueOf(this.getTotal_betrag()));
		pairs.put("b_shop", this.getShop());
		pairs.put("b_text", this.getText());
		pairs.put("b_datum", df.format(this.getDatum()));

		if (this.getFoto() != null) {
			pairs.put("b_foto", this.getFoto());
		}

		Map<Integer, Eigenanteil> betrag_aufteilung = this.getUserInklEigenanteile();
		String b_users = "", b_beitraege = "", b_beitraege_pfand = "";
		String sep = "";
		for (Entry<Integer, Eigenanteil> e : betrag_aufteilung.entrySet()) {
			b_users += sep + String.valueOf(e.getKey());
			b_beitraege += sep + String.valueOf(e.getValue().getBetrag());
			b_beitraege_pfand += sep + String.valueOf(e.getValue().getPfand());
			sep = ",";
		}

		pairs.put("b_users", b_users);
		pairs.put("b_beitraege", b_beitraege);
		pairs.put("b_beitraege_pfand", b_beitraege_pfand);

		return pairs;
	}

	@Override
	public boolean isSync() {
		return isSync;
	}

	public void setSync(boolean s) {
		this.isSync = s;
	}

}
