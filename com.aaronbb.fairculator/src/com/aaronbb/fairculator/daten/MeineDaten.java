package com.aaronbb.fairculator.daten;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.json.JSONObject;


public class MeineDaten extends Benutzer implements Serializable, SyncRequest {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9046640077541125622L;
	private String passwort;
	private boolean issync = false;
	//private ArrayList<Event> rechnungen;
	private Map<Integer, Event> events = new LinkedHashMap<Integer, Event>();
	private Map<Integer, Benutzer> kinder = new LinkedHashMap<Integer, Benutzer>();
	private Map<Integer, Benutzer> couser = new LinkedHashMap<Integer, Benutzer>();
	private ArrayList<String> shops = new ArrayList<String>();
	
	//Liste der HTTP-Anfragen, die noch ausstehen
	private LinkedList<HttpAnfrage> httpposts = new LinkedList<HttpAnfrage>();
	
	//Lokale Billids sind negativ und werden nur f�r noch nicht online gespeicherte Bills verwendet
	private int lastLocalBillId = -1;
	
	

	public MeineDaten (String email, String passwort){
		
		this.setEmail(email);
		this.setPasswort(passwort);
		//this.rechnungen = new ArrayList<Event>();
		this.setSync(false);
		
	}


	public MeineDaten() {
		//leerer Benutzer
		issync = false;
	}

	public MeineDaten(JSONObject jsonuser) {
		super(jsonuser);
	}
	
	public void update(MeineDaten couser){
		this.setName(couser.getName());
		this.setEmail(couser.getEmail());
		this.setId(couser.getId());
	}


	public void abmelden(){
		name = email = passwort =  "";
		issync = false;
		id = 0;
		events = null;
	}
	
	public int getAnzahlAnEvents(){
		return events.size();
	}
	
	private Map<Integer, Event> getEvents() {
		return events;
	}
	

	public Event getEvent(int id) {
		return events.get(id);
	}
	
	public Boolean hasEvent(int id) {
		return events.containsKey(id);
	}

	public void putEvent(Event event) {
		if (this.events.containsKey(event.getId())) {
			this.events.get(event.getId()).update(event);
	} else this.events.put(event.getId(), event);
	}


	public String getPasswort() {
		return passwort;
	}

	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}

	public boolean isVerified() {
		return issync;
	}

	public void setSync(boolean issync) {
		this.issync = issync;
	}

	public void clearEvents() {
		this.events.clear();
		
	}

	public ArrayList<String> getShops() {
		return shops;
	}

	public void addShop(String s) {
		this.shops.add(s);
	}

	public ArrayList<Integer> getPositionKeyTable(){
		return new ArrayList<Integer>(this.events.keySet());
	}

	public void putKind(Benutzer kind) {
		this.kinder.put(kind.getId(), kind);
	}

	public ArrayList<Integer> getKinderKeysByPosition() {
		return new ArrayList<Integer>(kinder.keySet());
	}

	public int sizeOfKinder() {
		return kinder.size();
	}
	
	public void removeKind(int id) {
		this.kinder.remove(id);
	}

	public Benutzer getKind(int user_id) {
		return kinder.get(user_id);
	}

	public void putCouser(Benutzer couser) {
		if(this.couser.containsKey(couser.getId())){
			this.couser.get(couser.getId()).update(couser);
		} else
			this.couser.put(couser.getId(), couser);
	}

	public int sizeOfCouser() {
		return couser.size();
	}
	
	public void removeCouser(int id) {
		this.couser.remove(id);
	}

	public Benutzer getCouser(int user_id) {
		return couser.get(user_id);
	}


//	public boolean hasCouser(int id) {
//		return this.couser.containsKey(id);
//	}


	public void clearShops() {
		shops.clear();
	}


	public boolean hasHttpAnfrage() {
		return !httpposts.isEmpty();
	}
	
	public HttpAnfrage getFirstHttpAnfrage() {
		return httpposts.poll();
	}


	public void addHttpAnfrage(HttpAnfrage httppost) {
		this.httpposts.add(httppost);
	}

	//erh�ht sie automatisch
	public int getLastLocalBillId() {
		return lastLocalBillId--;
	}


	@Override
	public String getQuery() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public HashMap<String, String> getPostVars() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public boolean isSync() {
		return issync;
	}
}
