package com.aaronbb.fairculator.daten;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.MissingResourceException;
import java.util.StringTokenizer;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.widget.ArrayAdapter;

public class Event implements Serializable, SyncRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6426089297530915159L;
	private String titel, text;
	private Date erstelltam;
	private int id, owner;
	private int size = 0;
	private boolean issync = false;
	private boolean saved = false;
	private Map<Integer, Benutzer> cousers = new LinkedHashMap<Integer, Benutzer>();
	private Map<Integer, Bill> bills = new LinkedHashMap<Integer, Bill>();
	private Locale locale = Locale.getDefault();

	public Event() {
		setTitel("");
		setErstelltam(new Date());
		setId(0);
		setSync(false);
	}

	public Event(JSONObject JSONevent) {
		try {
			setId((Integer) JSONevent.getInt("id"));
			setTitel(JSONevent.getString("titel"));
			setText(JSONevent.getString("text"));
			setOwner((Integer) JSONevent.getInt("owner"));
			if (JSONevent.has("currency") && !JSONevent.getString("currency").equals(""))
				setLocale(JSONevent.getString("currency"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		setSync(true);
	}

	public ArrayList<String> getCouserNameList() {
		ArrayList<String> liste = new ArrayList<String>();
		for (Entry<Integer, Benutzer> u : cousers.entrySet()) {
			liste.add(u.getValue().getName());
		}
		return liste;
	}

	public ArrayAdapter<Spanned> getCouserNameListArrayAdapter(Context c, int r) {
		ArrayAdapter<Spanned> listAdapter = new ArrayAdapter<Spanned>(c, r);
		for (Entry<Integer, Benutzer> u : cousers.entrySet()) {
			if (u.getKey() == owner) {
				listAdapter.add(Html.fromHtml("<b>" + u.getValue().getName() + "</b>"));
			} else
				listAdapter.add(Html.fromHtml(u.getValue().getName()));
		}
		return listAdapter;
	}

	public boolean removeCouser(int id) {
		if (cousers.remove(id) != null) {
			return true;
		} else
			return false;
	}

	public int getCouserIdByPosition(int pos) {
		int i = 0;
		for (Entry<Integer, Benutzer> u : cousers.entrySet()) {
			if (i == pos)
				return u.getKey();
			i++;
		}
		return 0;
	}

	public Map<Integer, Double> getGuthabenBei(int own_userid) {
		Map<Integer, Double> Guthaben = new HashMap<Integer, Double>();

		// Alle Bills durchgehen
		for (Bill b : bills.values()) {
			int besitzerdesbills = b.getOwner_id();
			if (besitzerdesbills == own_userid) {
				// Wenn man selbst den Bill bezahlt hat
				// Alle Eigenanteile durchgehen
				for (int id : b.getCouserIds()) {
					if (id != own_userid) {
						Double alterbetrag = 0.0;
						if (Guthaben.get(id) != null)
							alterbetrag = Guthaben.get(id);
						Guthaben.put(id, alterbetrag + b.getAnteilbyId(id));
					}
				}

			} else {
				// Bei Bills die andere bezahlt haben
				// Alle Eigenanteile durchgehen
				for (int id : b.getCouserIds()) {
					if (id == own_userid) {
						Double alterbetrag = 0.0;
						if (Guthaben.get(besitzerdesbills) != null)
							alterbetrag = Guthaben.get(besitzerdesbills);
						Guthaben.put(besitzerdesbills, alterbetrag - b.getAnteilbyId(own_userid));
					}
				}
			}
		}
		return Guthaben;
	}

	public int keyAt(int position) {
		int i = 0;
		for (Entry<Integer, Bill> e : bills.entrySet()) {
			if (i == position) {
				return e.getKey();
			}
			i = i + 1;
		}
		return 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getGekauftam() {
		return erstelltam;
	}

	public void setErstelltam(Date erstelltam) {
		this.erstelltam = erstelltam;
	}

	public String getTitel() {
		if (titel != null) {
			return titel;
		} else
			return "";
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public boolean isLoaded() {
		return issync;
	}

	public void setSync(boolean loaded) {
		this.issync = loaded;
	}

	public Bill getBill(int id) {
		return bills.get(id);
	}
	public void delBill(int id) {
		bills.remove(id);
	}

	public Map<Integer, Bill> getBills() {
		return bills;
	}

	public void putBill(Bill bill) {

		if (this.bills.containsKey(bill.getId())) {
			this.bills.get(bill.getId()).update(bill);
		} else
			this.bills.put(bill.getId(), bill);
	}

	public int getAnzahlAnRechungen() {
		if (this.bills.size() != 0) {
			return this.bills.size();
		} else
			return this.size;
	}

	public void setAnzahlAnRechnungen(int i) {
		this.size = i;
	}

	// public String getCouserName(int id) {
	// if (!cousers.containsKey(id)) {
	// return "n/a";
	// } else
	// return cousers.get(id).getName();
	// }

	public Benutzer getCouser(int id) {
		return cousers.get(id);
	}
	public Map<Integer, Benutzer> getCouserMap(){
		return cousers;
	}

	/*
	 * public Map<Integer, String> getCousers() { return cousers; }
	 */

	public String implodeCousers() {
		String str = "";
		String sep = "";
		for (Entry<Integer, Benutzer> e : cousers.entrySet()) {
			str += sep + String.valueOf(e.getKey());
			sep = ",";
		}
		return str;
	}

	// public void addCouser(int id, String newuser) {
	// MeinDaten b = new MeinDaten();
	// b.setId(id);
	// b.setName(newuser);
	// this.cousers.put(id, b);
	// }

	public void putCouser(Benutzer user) {
		this.cousers.put(user.getId(), user);
	}

	public int getOwner() {
		return owner;
	}

	public void setOwner(int owner) {
		this.owner = owner;
	}

	public String getText() {
		if (text != null) {
			return text;
		} else
			return "";
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isSaved() {
		return saved;
	}

	public void setSaved(boolean saved) {
		this.saved = saved;
	}

	public ArrayList<Integer> getPositionKeyTable() {
		return new ArrayList<Integer>(this.bills.keySet());
	}

	public Locale getLocale() {
		return locale;
	}

	public String getLocaleString() {
		return this.locale.getLanguage() + "_" + this.locale.getCountry();
	}

	public void setLocale(Locale currency) {
		this.locale = currency;
	}

	public void setLocale(String loc_str) {
		String[] loc_str_parts = loc_str.split("_");
		try {
			this.locale = new Locale(loc_str_parts[0], loc_str_parts[1]);
		} catch (Exception e1) {
		}
	}

	public void clearCouser() {
		cousers.clear();
	}

	public void update(Event aktuellesevent) {
		this.setTitel(aktuellesevent.getTitel());
		this.setText(aktuellesevent.getText());
		this.setOwner(aktuellesevent.getOwner());
		this.setLocale(aktuellesevent.getLocale());
		this.setSync(aktuellesevent.isLoaded());
	}

	public boolean hasBill(int bill_id) {
		
		return bills.containsKey(bill_id);
	}

	@Override
	public String getQuery() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<String, String> getPostVars() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isSync() {
		// TODO Auto-generated method stub
		return false;
	}

}
