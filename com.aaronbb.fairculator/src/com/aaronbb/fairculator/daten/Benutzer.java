package com.aaronbb.fairculator.daten;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Benutzer implements Serializable {
	protected String name;
	protected String email;
	protected int id;
	protected int owner;
	private boolean loeschen = false;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Benutzer() {
		super();
	}

	public Benutzer(JSONObject jsonuser) {
		try {
			setId(jsonuser.getInt("id"));
			setName(jsonuser.getString("name"));
			if (jsonuser.has("email"))
				setEmail(jsonuser.getString("email"));
			if (jsonuser.has("owner"))
				setOwner(jsonuser.getInt("owner"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getName() {
		return this.name;
	}

	public void setName(String string) {
		this.name = string;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setOwner(int id) {
		this.owner = id;
	}

	public int getOwner() {
		return this.owner;
	}

	public boolean isLoeschen() {
		return loeschen;
	}

	public void setLoeschen(boolean loeschen) {
		this.loeschen = loeschen;
	}
	
	public void update(Benutzer couser){
		this.setName(couser.getName());
		this.setEmail(couser.getEmail());
		this.setOwner(couser.getOwner());
	}

}
