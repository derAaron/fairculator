package com.aaronbb.fairculator.daten;

import java.io.Serializable;
import java.util.HashMap;

public class HttpAnfrage implements Serializable{

	String url;

	HashMap<String, String> post_vars;
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public HashMap<String, String> getPost_vars() {
		return post_vars;
	}

	public void setPost_vars(HashMap<String, String> post_vars) {
		this.post_vars = post_vars;
	}

	private static final long serialVersionUID = 4867468587335474777L;
	public HttpAnfrage(String url) {
		super();
		this.url = url;
		this.post_vars = null;
	}
	
	public HttpAnfrage(String url, HashMap<String, String> post_vars) {
		super();
		this.url = url;
		this.post_vars = post_vars;
	}

	public void kill() {
		this.url = null;
		this.post_vars = null;
	}
}