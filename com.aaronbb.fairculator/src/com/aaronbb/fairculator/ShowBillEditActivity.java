package com.aaronbb.fairculator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.aaronbb.fairculator.daten.Benutzer;
import com.aaronbb.fairculator.daten.Bill;
import com.aaronbb.fairculator.daten.Bill.Eigenanteil;
import com.aaronbb.fairculator.daten.Event;
import com.aaronbb.fairculator.daten.HttpAnfrage;
import com.aaronbb.fairculator.daten.MeineDaten;
import com.aaronbb.fairculator.extra.MoneyKeyboard;
import com.aaronbb.fairculator.extra.RoundedImageView;
import com.google.analytics.tracking.android.EasyTracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class ShowBillEditActivity extends FairculatorActivity implements OnItemSelectedListener {

	private static final int D_DATE_PICKER = 0;
	private static final int D_USER_PICKER = 1;
	private static final int D_EDIT_EIGENANTEIL = 2;

	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	private static final int EDIT_EIGENANTEIL_ACTIVITY_REQUEST_CODE = 101;
	private Uri mCapturedImageURI;

	// Datentr�ger
	private int bill_id, event_id;
	private Bill bill_kopie;
	private Event aktuellesevent;

	// Editfelder festlegen
	private AutoCompleteTextView ShowBillShopEdit;
	private EditText ShowBillTextEdit, ShowBillTotalEdit, ShowBillDatumEdit;
	// private LinearLayout ShowBillEditEinanteilDialog;
	private RoundedImageView ShowBillEditPicThumb;
	private Map<Integer, CheckBox> CB_map;
	private Map<Integer, ImageView> IV_map;
	private Map<Integer, TextView> TV_map, TVea_map;
	private int edit_eigenanteil_selected_user = 0;

	private MoneyKeyboard mCustomKeyboard;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Bill und Event ID laden
		event_id = sharedprefs.getInt("event_id", 0);
		bill_id = sharedprefs.getInt("bill_id", 0);

		PreferenceManager.getDefaultSharedPreferences(this);

		// Show the Up button in the action bar.
		setupActionBar();

	}

	public void onCreateView() {

		if (aktuellesevent == null)
			aktuellesevent = benutzer.getEvent(event_id);

		// Wenn der Bill noch nicht geladen ist
		if (bill_kopie == null) {
			if (bill_id != 0) {
				// den aktuellen Bill aus den Daten kopieren
				bill_kopie = new Bill(benutzer, aktuellesevent, bill_id);
				bill_kopie.update(benutzer.getEvent(event_id).getBill(bill_id));
			} else {
				// sonst einen leeren mit einer lokalen Id erzeugen
				bill_kopie = new Bill(benutzer, aktuellesevent, benutzer.getLastLocalBillId());
			}
		}

		setContentView(R.layout.activity_show_bill_edit);

		ShowBillShopEdit = (AutoCompleteTextView) findViewById(R.id.ShowBillShopEdit);
		ShowBillTextEdit = (EditText) findViewById(R.id.ShowBillTextEdit);
		ShowBillTotalEdit = (EditText) findViewById(R.id.ShowBillTotalEdit);
		ShowBillDatumEdit = (EditText) findViewById(R.id.ShowBillDatumEdit);
		ShowBillEditPicThumb = (RoundedImageView) findViewById(R.id.ShowBillEditPicThumb);

		setTitle(getString(R.string.title_activity_show_bill)
				+ (bill_kopie.getShop() != null ? ": " + bill_kopie.getShop() : ""));

		// MoneyKeyboard initalisieren
		mCustomKeyboard = new MoneyKeyboard(this, R.id.keyboardview, R.xml.money_keyboard,
				String.valueOf((new DecimalFormatSymbols(aktuellesevent.getLocale())).getDecimalSeparator()));
		mCustomKeyboard.registerEditText(R.id.ShowBillTotalEdit);

		// Werte in Felder laden
		ShowBillShopEdit.setText(bill_kopie.getShop());
		ShowBillShopEdit.setAdapter(new ArrayAdapter<String>(ShowBillEditActivity.this,
				android.R.layout.simple_dropdown_item_1line, benutzer.getShops().toArray(
						new String[benutzer.getShops().size()])));
		ShowBillShopEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View view, boolean hasFocus) {
				if (hasFocus) {
					ShowBillShopEdit.showDropDown();
				}
			}
		});
		ShowBillShopEdit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (ShowBillShopEdit.getText().length() == 0)
					ShowBillShopEdit.showDropDown();
			}
		});
		ShowBillShopEdit.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (!bill_kopie.setShop(ShowBillShopEdit.getText().toString()))
					showError(ShowBillShopEdit, getString(R.string.e_bitteangeben));
			}
		});

		ShowBillTextEdit.setText(bill_kopie.getText());
		ShowBillTextEdit.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				bill_kopie.setText(ShowBillTextEdit.getText().toString());
			}
		});

		ShowBillTotalEdit.setText(GeldFormatOhneE(aktuellesevent.getLocale()).format(bill_kopie.getTotal_betrag()));
		ShowBillTotalEdit.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (!bill_kopie.setTotal_betrag(ShowBillTotalEdit.getText().toString(), aktuellesevent.getLocale()))
					showError(ShowBillTotalEdit, getString(R.string.e_keingeldbetrag));
				aufteilungAktualisieren();
			}
		});
		ShowBillTotalEdit.setOnEditorActionListener(new OnEditorActionListener() {
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEND) {
					speichern();
					return true;
				}
				return false;
			}
		});
		// ShowBillTotalEdit.setOnFocusChangeListener(new
		// OnFocusChangeListener() {
		//
		// @Override
		// public void onFocusChange(View v, boolean hasFocus) {
		// if(hasFocus){
		// InputMethodManager imm = (InputMethodManager)getSystemService(
		// Context.INPUT_METHOD_SERVICE);
		// imm.hideSoftInputFromWindow(ShowBillTotalEdit.getWindowToken(), 0);
		// }
		//
		// }
		// });

		ShowBillDatumEdit.setText(dateFormat.format(bill_kopie.getDatum()));
		ShowBillDatumEdit.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (v == ShowBillDatumEdit)
					showDialog(D_DATE_PICKER);
				return true;
			}
		});

		// Alle Couser mit Beitrag auflisten
		TableLayout ShowBillAufteilung = (TableLayout) findViewById(R.id.ShowBillAufteilung);
		ShowBillAufteilung.removeAllViewsInLayout();

		CB_map = new HashMap<Integer, CheckBox>();
		TVea_map = new HashMap<Integer, TextView>();
		TV_map = new HashMap<Integer, TextView>();
		IV_map = new HashMap<Integer, ImageView>();

		LayoutInflater inflater = (LayoutInflater) ShowBillEditActivity.this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		Boolean billistneu = (bill_kopie.getUserInklEigenanteile().size() == 0) ? true : false;
		for (Integer cu_id : bill_kopie.getCouserIds()) {
			View vi = inflater.inflate(R.layout.list_betraege_row_edit, null);
			vi.setId(cu_id);

			// Checkbox mit Namen belegen
			CheckBox CBname = (CheckBox) vi.findViewById(R.id.show_bill_list_edit_user);
			CBname.setText(bill_kopie.getCouser(cu_id).getName());
			CBname.setId(cu_id);

			ImageView IVicon = (ImageView) vi.findViewById(R.id.ShowBillEditListEditIcon);

			// Eigenanteils TV zuweisen
			TextView TVeigenanteil = (TextView) vi.findViewById(R.id.ShowBillEditListEigenanteil);
			TVeigenanteil.setId(cu_id);

			// Anteils TV zuweisen
			TextView TVanteil = (TextView) vi.findViewById(R.id.show_bill_list_anteil);
			TVanteil.setId(cu_id);

			// Anteil anzeigen
			// TVanteil.setText(StartActivity.GeldFormat.format(bill.getAnteilbyId(cu.getKey())));

			// Wenn Nutzer nicht in users-liste des bills ist, wird er nicht
			// angehakt sein
			if (bill_kopie.isAmEinkaufBeteiling(cu_id)) {
				CBname.setChecked(true);
				TVanteil.setVisibility(View.VISIBLE);
			} else {
				CBname.setChecked(false);
				TVanteil.setVisibility(View.INVISIBLE);
			}

			// Wenn ein Haken gesetzt wird soll das zugeh�rige Feld aktiviert
			// werden
			CBname.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (isChecked) {
						TV_map.get(buttonView.getId()).setVisibility(View.VISIBLE);
						bill_kopie.putUserInklEigenanteil(bill_kopie.getCouser(buttonView.getId()), 0.0, 0.0);
					} else {
						TV_map.get(buttonView.getId()).setVisibility(View.INVISIBLE);
						bill_kopie.delUserInklEigenanteil(buttonView.getId());
					}
					// Alle TVs aktualisieren
					aufteilungAktualisieren();
				}
			});

			// Clicklistener initialisieren
			vi.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					edit_eigenanteil_selected_user = v.getId();
					if (CB_map.get(edit_eigenanteil_selected_user).isChecked()) {
						// showDialog(D_EDIT_EIGENANTEIL);
						startActivityForResult(
								new Intent(getBaseContext(), ShowBillEditEigenanteilActivity.class)
										.putExtra("bill", bill_kopie).putExtra("locale", aktuellesevent.getLocale())
										.putExtra("selected_user", edit_eigenanteil_selected_user),
								EDIT_EIGENANTEIL_ACTIVITY_REQUEST_CODE);
					}
				}
			});

			// Referenz speichern
			CB_map.put(cu_id, CBname);
			TV_map.put(cu_id, TVanteil);
			TVea_map.put(cu_id, TVeigenanteil);
			IV_map.put(cu_id, IVicon);

			// Zeile und Trenner anzeigen
			ShowBillAufteilung.addView(vi);
			View ruler = new View(getBaseContext());
			ruler.setBackgroundColor(0x11111100);
			ShowBillAufteilung.addView(ruler, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2));
		}
		// Anteile anzeigen
		aufteilungAktualisieren();

		// Wenn User owner des Events ist darf der den Zahler w�hlen, sonst ist
		// es immer der User selbst
		if (benutzer.getId() == aktuellesevent.getOwner()) {
			Spinner userpicker = (Spinner) findViewById(R.id.ShowBillUserPicker);

			int couserSize = bill_kopie.getCouserSize();
			String[] couserNames = new String[couserSize];
			Integer[] couserIds = new Integer[couserSize];
			Integer currentItem = 0;
			int i = 0;
			for (Integer e : bill_kopie.getCouserIds()) {
				couserNames[i] = bill_kopie.getCouser(e).getName();
				couserIds[i] = e;
				if (e == bill_kopie.getOwner_id())
					currentItem = i;
				i++;
			}
			ArrayAdapter<String> userpicker_adapter = new UserPickerAdapter(ShowBillEditActivity.this,
					android.R.layout.simple_spinner_item, couserNames, couserIds);
			userpicker_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			userpicker.setAdapter(userpicker_adapter);
			userpicker.setSelection(currentItem);
			userpicker.setVisibility(View.VISIBLE);
			findViewById(R.id.ShowBillEditZahler).setVisibility(View.VISIBLE);
			userpicker.setOnItemSelectedListener(ShowBillEditActivity.this);
		}

		if (bill_kopie.getFotoChecksum() != null && bill_kopie.getFoto() == null) {
			ShowBillEditPicThumb.setBackgroundDrawable(null);
			ImageLoader.getInstance().displayImage(
					LOAD_BILL_PIC_URL + "?email=" + benutzer.getEmail() + "&pass=" + benutzer.getPasswort()
							+ "&action=loadbill&bill_id=" + bill_id + "&thumb&cs=" + bill_kopie.getFotoChecksum(),
					ShowBillEditPicThumb,
					new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.ic_action_warning)
							.showImageOnFail(R.drawable.ic_action_error).cacheInMemory(true).cacheOnDisc(true)
							.considerExifParams(true).imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
							.imageScaleType(ImageScaleType.EXACTLY_STRETCHED).build(),
					new SimpleImageLoadingListener() {

						@Override
						public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
							String message = null;
							switch (failReason.getType()) {
								case IO_ERROR:
									message = "Input/Output error";
									break;
								case DECODING_ERROR:
									message = "Image can't be decoded";
									break;
								case NETWORK_DENIED:
									message = "Downloads are denied";
									break;
								case OUT_OF_MEMORY:
									message = "Out Of Memory error";
									break;
								case UNKNOWN:
									message = "Unknown error";
									break;
							}
							Toast.makeText(ShowBillEditActivity.this, message, Toast.LENGTH_SHORT).show();
						}
					});
		} else {
			if (bill_kopie.getFoto() != null)
				ShowBillEditPicThumb.setImageBitmap(bill_kopie.getFotoBitmap());
		}
		ShowBillEditPicThumb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (bill_kopie.getFotoChecksum() == null) {
					startCamera();
				} else
					startActivity(new Intent(getBaseContext(), ShowBillPictureActivity.class).putExtra("foto_checksum",
							bill_kopie.getFotoChecksum()));

			}
		});
		registerForContextMenu(ShowBillEditPicThumb);
	}

	protected void speichern() {
		// Bill bearbeiten und Fehler suchen
		boolean fehlergefunden = false;
		if (!bill_kopie.setShop(ShowBillShopEdit.getText().toString()))
			fehlergefunden = showError(ShowBillShopEdit, getString(R.string.e_bitteangeben));
		// if(!bill.setTotal_betrag(ShowBillTotalEdit.getText().toString()))
		// fehlergefunden = showError(ShowBillTotalEdit,
		// getString(R.string.e_keingeldbetrag));
		if (bill_kopie.sizeOfUserInklEigenanteile() == 0)
			for (Entry<Integer, CheckBox> cb : CB_map.entrySet())
				fehlergefunden = showError(cb.getValue(), getString(R.string.e_wenistenseinuser));

		if (bill_kopie.getGemBetrag() < 0)
			fehlergefunden = showError(ShowBillTotalEdit,
					getString(R.string.e_diesummedereigenanteileuebersteigtdiegesamtsumme));

		// Bill an Server schicken
		if (!fehlergefunden) {
			bill_kopie.setSync(false);
			benutzer.getEvent(event_id).putBill(bill_kopie);
			new Loader(true).execute(true);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		new Loader(false).execute();
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.show_bill_edit, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.ShowBillEditSave:
				speichern();
				return true;

			case R.id.show_bill_edit_abmelden:
				SharedPreferences.Editor prefEditor = sharedprefs.edit();
				prefEditor.remove("email");
				prefEditor.remove("pass");
				prefEditor.commit();
				benutzer.abmelden();
				Intent i = new Intent(getBaseContext(), StartActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
				return true;

			case R.id.show_bill_edit_shortcut:
				addShortcut();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		switch (id) {
			case D_DATE_PICKER:
				Calendar c = Calendar.getInstance();
				int cyear = c.get(Calendar.YEAR);
				int cmonth = c.get(Calendar.MONTH);
				int cday = c.get(Calendar.DAY_OF_MONTH);
				return new DatePickerDialog(this, mDateSetListener, cyear, cmonth, cday);

				// case D_USER_PICKER:
				// if (!couser.isEmpty()) {
				// AlertDialog.Builder builder = new AlertDialog.Builder(this);
				// builder.setTitle(getString(R.string.d_zahler));
				//
				// int couserSize = couser.size();
				// CharSequence[] couserNames = new CharSequence[couserSize];
				// final Integer[] couserIds = new Integer[couserSize];
				// int i = 0;
				// for (Entry<Integer, String> e : couser.entrySet()) {
				// couserNames[i] = (CharSequence) e.getValue();
				// couserIds[i] = e.getKey();
				// i++;
				// }
				//
				// builder.setItems(couserNames,
				// new DialogInterface.OnClickListener() {
				//
				// @Override
				// public void onClick(DialogInterface dialog,
				// int which) {
				// // TODO Auto-generated method stub
				// bill.setOwner_id(couserIds[which]);
				// }
				// });
				// return builder.create();
				// }

		}
		return null;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.show_bill_edit_pic_context, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.show_bill_edit_pic_context_anzeigen:
				startActivity(new Intent(getBaseContext(), ShowBillPictureActivity.class).putExtra("foto_checksum",
						bill_kopie.getFotoChecksum()));
				return true;
			case R.id.show_bill_edit_pic_context_neu:
				startCamera();
				return true;
			case R.id.show_bill_edit_pic_context_loeschen:
				ShowBillEditPicThumb.setBackgroundResource(R.drawable.ab_background_textured_fairculator);
				ShowBillEditPicThumb.setImageResource(R.drawable.ic_action_new_picture);
				bill_kopie.setFoto("");
				return true;
			default:
				return super.onContextItemSelected(item);
		}

	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		// onDateSet method
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			String date_selected = dateFormat.format(new Date(year - 1900, monthOfYear, dayOfMonth));
			// Toast.makeText(getApplicationContext(),
			// "Selected Date is ="+date_selected, Toast.LENGTH_SHORT).show();
			ShowBillDatumEdit.setText(date_selected);
			if (!bill_kopie.setDatum(date_selected))
				showError(ShowBillDatumEdit, getString(R.string.e_datumfalsch));
		}
	};

	public class Loader extends AsyncTask<Boolean, String, Boolean> {
		ProgressDialog progDailog;
		boolean finish_after_loading = false;
		

		public Loader(boolean save) {
			super();
			this.finish_after_loading = save;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDailog = new ProgressDialog(ShowBillEditActivity.this);
			progDailog.setMessage(getString(R.string.d_laden));
			progDailog.setIndeterminate(false);
			progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDailog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					Loader.this.cancel(true);
					finish();
				}
			});
			progDailog.show();
		}

		protected Boolean doInBackground(Boolean... auchspeichern) {

			if (handleSyncRequest(bill_kopie)) {
				if (bill_kopie.getId() > 0)
					bill_kopie.update(benutzer.getEvent(bill_kopie.getEvent_id()).getBill(bill_kopie.getId()));
				//bill_kopie.update(benutzer.getEvent(event_id).getBill(bill_id));
				return true;
			} else
				return false;

		}

		protected void onPostExecute(Boolean result) {
			if (progDailog.isShowing()) {
				progDailog.dismiss();
			}
			if (!result && (!benutzer.hasEvent(event_id))) {
				startActivity((new Intent(getBaseContext(), StartActivity.class))
						.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				return;
			}

			if (benutzer.isVerified()) {

				// Wenn Bill gespechert wurde Toast anzeigen
				if (this.finish_after_loading) {
					Toast.makeText(getBaseContext(), getString(R.string.t_gespeichert), Toast.LENGTH_LONG).show();
					this.finish_after_loading = false;
					finish();
				}

				onCreateView();

			} else {
				Intent intent = new Intent(getBaseContext(), StartActivity.class);
				startActivity(intent);
			}
		}
	}

	private void aufteilungAktualisieren() {
		for (Entry<Integer, TextView> tv : TV_map.entrySet()) {
			tv.getValue().setText(GeldFormat(aktuellesevent.getLocale()).format(bill_kopie.getAnteilbyId(tv.getKey())));
			Double eigenanteil = bill_kopie.getEigenanteilById(tv.getKey());
			Double eigenpfand = bill_kopie.getEigenPfandanteilById(tv.getKey());
			if ((eigenanteil != null && eigenanteil != 0.0) || (eigenpfand != null && eigenpfand != 0.0)) {
				String text = "(";
				if (eigenpfand != null && eigenpfand != 0.0)
					text += "<font color=\"" + String.format("#%06X", (0xFFFFFF & FARBE_WALD_GRUEN)) + "\">"
							+ GeldFormat(aktuellesevent.getLocale()).format(eigenpfand) + "</font>";
				if ((eigenanteil != null && eigenanteil != 0.0) && (eigenpfand != null && eigenpfand != 0.0))
					text += " / ";
				if (eigenanteil != null && eigenanteil != 0.0)
					text += "<font color=\"" + String.format("#%06X", (0xFFFFFF & FARBE_WALD_ROT)) + "\">"
							+ GeldFormat(aktuellesevent.getLocale()).format(eigenanteil) + "</font>";
				text += ")";
				TVea_map.get(tv.getKey()).setText(Html.fromHtml(text));
			} else {
				TVea_map.get(tv.getKey()).setText("");
			}

			if (bill_kopie.isAmEinkaufBeteiling(tv.getKey())) {
				IV_map.get(tv.getKey()).setVisibility(View.VISIBLE);
			} else
				IV_map.get(tv.getKey()).setVisibility(View.INVISIBLE);
		}
	}

	private void addShortcut() {
		Intent shortcutIntent = new Intent(getApplicationContext(), StartActivity.class);
		shortcutIntent.setAction(Intent.ACTION_MAIN);
		shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		shortcutIntent.putExtra("event_id", event_id);
		shortcutIntent.putExtra("bill_id", 0);
		shortcutIntent.putExtra("shortcut", "new_bill");

		Intent addIntent = new Intent();
		addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
		addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, aktuellesevent.getTitel() + getString(R.string.shortcut_trenner)
				+ getString(R.string.shortcut_neuerechnung));
		addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
				Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.drawable.ic_launcher));

		addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
		getApplicationContext().sendBroadcast(addIntent);
		Toast.makeText(getBaseContext(), getString(R.string.t_shortcut_hinzugefuegt), Toast.LENGTH_LONG).show();
	}

	private boolean showError(EditText ETreferenz, String fehlertext) {
		Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
		ETreferenz.startAnimation(shake);
		ETreferenz.setError(fehlertext);
		ETreferenz.requestFocus();
		return true;
	}

	private boolean showError(CheckBox ETreferenz, String fehlertext) {
		Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
		ETreferenz.startAnimation(shake);
		ETreferenz.setError(fehlertext);
		ETreferenz.requestFocus();
		return true;
	}

	private void startCamera() {
		String fileName = "temp.jpg";
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		mCapturedImageURI = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		/*
		 * File file = new File(Environment.getExternalStorageDirectory() +
		 * "/DCIM/", "fctemp.jpg"); mCapturedImageURI = Uri.fromFile(file);
		 */

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
		startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;

		if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				if (data != null && mCapturedImageURI == null) {
					mCapturedImageURI = data.getData();
				}
				Bitmap b = getBitmap(mCapturedImageURI);
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				b.compress(Bitmap.CompressFormat.JPEG, 60, stream);
				byte[] byteArray = stream.toByteArray();
				bill_kopie.setFoto(Base64.encodeToString(byteArray, 0));
				bill_kopie.setFotoChecksum(null); // Checksum l�schen, da neues
													// Bild
				// nicht mehr zur alten Checksum
				// passt und somit beim erneuten
				// tippen auf das Foto die Kamera
				// wieder ge�ffnet wird

				ShowBillEditPicThumb.setBackgroundDrawable(null);
				ShowBillEditPicThumb.setImageBitmap(b);

				getContentResolver().delete(mCapturedImageURI, null, null);
			} else if (resultCode == Activity.RESULT_CANCELED) {
				// User cancelled the image capture
				getContentResolver().delete(mCapturedImageURI, null, null);
			} else {
				// Image capture failed, advise user
			}
		}
		if (requestCode == EDIT_EIGENANTEIL_ACTIVITY_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				if (data != null) {
					Bundle extras = data.getExtras();
					if (extras != null) {
						bill_kopie = (Bill) extras.getSerializable("bill");
						aufteilungAktualisieren();
					}
				}
			} else if (resultCode == Activity.RESULT_CANCELED) {
			} else {
			}
		}
	}

	private Bitmap getBitmap(Uri uri) {
		InputStream in = null;
		try {
			final int IMAGE_MAX_SIZE = 500000; // 1.2MP
			in = getContentResolver().openInputStream(uri);

			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(in, null, o);
			in.close();

			int scale = 1;
			while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) > IMAGE_MAX_SIZE) {
				scale++;
			}

			Bitmap b = null;
			in = getContentResolver().openInputStream(uri);

			if (scale > 1) {
				scale--;
				o = new BitmapFactory.Options();
				o.inSampleSize = scale;
				b = BitmapFactory.decodeStream(in, null, o);

				// resize to desired dimensions
				int height = b.getHeight();
				int width = b.getWidth();

				double y = Math.sqrt(IMAGE_MAX_SIZE / (((double) width) / height));
				double x = (y / height) * width;

				Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x, (int) y, true);
				b.recycle();
				b = scaledBitmap;

				System.gc();
			} else {
				b = BitmapFactory.decodeStream(in);
			}
			in.close();

			Matrix matrix = new Matrix();
			matrix.postRotate(getImageOrientation(uri));
			Bitmap rotatedBitmap = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
			return rotatedBitmap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static int getImageOrientation(Uri imagePath) {
		int rotate = 0;
		try {
			ExifInterface exif = new ExifInterface(imagePath.getPath());
			int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

			switch (orientation) {
				case ExifInterface.ORIENTATION_ROTATE_270:
					rotate = 270;
					break;
				case ExifInterface.ORIENTATION_ROTATE_180:
					rotate = 180;
					break;
				case ExifInterface.ORIENTATION_ROTATE_90:
					rotate = 90;
					break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return rotate;
	}

	private class UserPickerAdapter extends ArrayAdapter<String> {
		Integer[] ids;

		public UserPickerAdapter(Context context, int resource, String[] objects, Integer[] ids) {
			super(context, resource, objects);
			this.ids = ids;
		}

		public Integer getIdAt(int position) {
			return ids[position];
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		bill_kopie.setOwner_id(((UserPickerAdapter) parent.getAdapter()).getIdAt(position));
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBackPressed() {
		// NOTE Trap the back key: when the CustomKeyboard is still visible hide
		// it, only when it is invisible, finish activity
		if (mCustomKeyboard.isCustomKeyboardVisible())
			mCustomKeyboard.hideCustomKeyboard();
		else
			this.finish();
	}
}
