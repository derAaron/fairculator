package com.aaronbb.fairculator;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase.DisplayType;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.aaronbb.fairculator.daten.MeineDaten;
import com.google.analytics.tracking.android.EasyTracker;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class ShowBillPictureActivity extends FairculatorActivity {
	

	private int bill_id;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_show_bill_picture);

		final ImageViewTouch bild = (ImageViewTouch) findViewById(R.id.ShowBillPicture);

        
		//Bill ID laden
        bill_id = sharedprefs.getInt("bill_id", 0);
        
        //MD5 laden
        String foto_checksum;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
            	foto_checksum= null;
            } else {
            	foto_checksum= extras.getString("foto_checksum");
            }
        } else {
        	foto_checksum= (String) savedInstanceState.getSerializable("foto_checksum");
        }
        		
		try {
			final ProgressBar spinner = (ProgressBar) findViewById(R.id.ShowBillPictureLoading);
			
			ImageLoader.getInstance().displayImage(LOAD_BILL_PIC_URL+"?email="+benutzer.getEmail()+"&pass="+benutzer.getPasswort()+"&action=loadbill&bill_id="+bill_id+"&cs="+foto_checksum, bild, new DisplayImageOptions.Builder()
			.showImageForEmptyUri(R.drawable.ic_action_edit)
			.showImageOnFail(R.drawable.ic_action_discard)
			.resetViewBeforeLoading(true)
			.cacheOnDisc(true)
			.imageScaleType(ImageScaleType.EXACTLY)
			.bitmapConfig(Bitmap.Config.RGB_565)
			.considerExifParams(true)
			.displayer(new FadeInBitmapDisplayer(300))
			.build(), new SimpleImageLoadingListener() {
				@Override
				public void onLoadingStarted(String imageUri, View view) {
				spinner.setVisibility(View.VISIBLE);
				}

				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
				String message = null;
				switch (failReason.getType()) {
				case IO_ERROR:
				message = "Input/Output error"+imageUri;
				break;
				case DECODING_ERROR:
				message = "Image can't be decoded";
				break;
				case NETWORK_DENIED:
				message = "Downloads are denied";
				break;
				case OUT_OF_MEMORY:
				message = "Out Of Memory error";
				break;
				case UNKNOWN:
				message = "Unknown error";
				break;
				}
				Toast.makeText(ShowBillPictureActivity.this, message, Toast.LENGTH_SHORT).show();

				spinner.setVisibility(View.GONE);
				}

				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				spinner.setVisibility(View.GONE);
				}
				});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		bild.setDisplayType(DisplayType.FIT_TO_SCREEN);
	}
}
