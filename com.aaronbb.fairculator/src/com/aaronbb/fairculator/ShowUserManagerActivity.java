package com.aaronbb.fairculator;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.aaronbb.fairculator.daten.Benutzer;
import com.aaronbb.fairculator.daten.MeineDaten;
import com.google.analytics.tracking.android.EasyTracker;

public class ShowUserManagerActivity extends Activity {
	// IDs
	private static final int DIALOG_EDIT = 1;
	public static final int REQUESTCODE_USER_MANAGER = 43634;

	private static MeineDaten benutzer;
	private SharedPreferences sharedprefs;

	private ListView listview;
	static AlertDialog dialog;

	private static Benutzer benutzer_ausgewaehlt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Benutzeremail und Passwort laden
		sharedprefs = getSharedPreferences("fairculator", MODE_PRIVATE);
		benutzer = new MeineDaten(sharedprefs.getString("email", ""), sharedprefs.getString("pass", ""));

		if (savedInstanceState == null) {
			new Loader().execute();
		}
	}

	public void writeScreen() {
		setContentView(R.layout.activity_show_user_manager);
		listview = (ListView) findViewById(R.id.ShowUserManagerList);
		listview.setAdapter(new ListAdapter(this));
		listview.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long arg3) {
				benutzer_ausgewaehlt = benutzer.getKind((Integer) parent.getAdapter().getItem(position));
				showDialog(DIALOG_EDIT);
				return true;
			}
		});
		if (this.getIntent().getIntExtra("REQUESTCODE", 0) == REQUESTCODE_USER_MANAGER) {
			listview.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {
					setResult(Activity.RESULT_OK,
							new Intent().putExtra("selected_user_id", (Integer) parent.getAdapter().getItem(position)));
					finish();
				}
			});
		}
		if (benutzer.sizeOfKinder() == 0)
			makenewuser();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.show_user_manager, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
			case R.id.ShowUserManagerMenuNeuerBenutzer:
				makenewuser();
				break;

			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
			case DIALOG_EDIT:
				View dialog_view = getLayoutInflater().inflate(R.layout.dialog_user_manager_edit, null);
				final AutoCompleteTextView dialog_view_email = (AutoCompleteTextView) dialog_view
						.findViewById(R.id.ShowUserManagerEditEmail);
				final EditText dialog_view_name = (EditText) dialog_view.findViewById(R.id.ShowUserManagerEditName);

				ArrayList<String> emailAddressCollection = new ArrayList<String>();
				Cursor emailCur = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
						null, null, null);
				while (emailCur.moveToNext())
					emailAddressCollection.add(emailCur.getString(emailCur
							.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)));
				emailCur.close();
				String[] emailAddresses = new String[emailAddressCollection.size()];
				emailAddressCollection.toArray(emailAddresses);

				ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
						android.R.layout.simple_dropdown_item_1line, emailAddresses);

				dialog_view_email.setAdapter(adapter);
				dialog_view_email.setOnEditorActionListener(new OnEditorActionListener() {
					public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_DONE) {
							if (speichern(dialog_view_name, dialog_view_email)) {
								dialog.dismiss();
							}
							return true;
						}
						return false;
					}
				});
				dialog = new AlertDialog.Builder(this).setTitle(R.string.d_benutzerbearbeiten).setView(dialog_view)
						.setPositiveButton(R.string.d_ok, null).setNegativeButton(R.string.d_abbrechen, null)
						.setNeutralButton(R.string.d_loeschen, null).create();
				dialog.setOnShowListener(new DialogInterface.OnShowListener() {
					@Override
					public void onShow(DialogInterface d) {
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.showSoftInput(dialog_view_name, InputMethodManager.SHOW_IMPLICIT);
						dialog_view_name.setError(null);
						dialog_view_name.requestFocus();
						dialog_view_name.setText(benutzer_ausgewaehlt.getName());
						dialog_view_email.setText(benutzer_ausgewaehlt.getEmail());
						if (benutzer_ausgewaehlt.getId() == 0) {
							dialog.setTitle(R.string.m_neuerbenutzer);
							dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.GONE);
						} else {
							dialog.setTitle(R.string.d_benutzerbearbeiten);
							dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.VISIBLE);
						}

						Button b = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
						b.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View view) {
								if (speichern(dialog_view_name, dialog_view_email)) {
									dialog.dismiss();
								}
							}
						});

						Button l = dialog.getButton(AlertDialog.BUTTON_NEUTRAL);
						l.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View view) {
								if (loeschen()) {
									dialog.dismiss();
								}
							}
						});
					}
				});

				return dialog;

			default:
				break;
		}
		return super.onCreateDialog(id);
	}

	private void makenewuser() {
		benutzer_ausgewaehlt = new MeineDaten();
		showDialog(DIALOG_EDIT);
	}

	protected Boolean speichern(EditText n, EditText e) {
		Boolean fehlergefunden = false;

		if (n.length() == 0)
			fehlergefunden = showError(n, getString(R.string.e_bitteangeben));

		if (!fehlergefunden) {
			benutzer_ausgewaehlt.setName(n.getText().toString());
			benutzer_ausgewaehlt.setEmail(e.getText().toString());
			new Loader().execute(true);
			n.setText(null);
			e.setText(null);
		}
		// true wenn alles OK und false wenn Fehler
		return !fehlergefunden;
	}

	private boolean loeschen() {
		benutzer_ausgewaehlt.setLoeschen(true);
		benutzer.removeKind(benutzer_ausgewaehlt.getId());
		new Loader().execute(true);
		return true;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (benutzer != null)
			outState.putSerializable("benutzer", benutzer);
		if (benutzer_ausgewaehlt != null)
			outState.putSerializable("benutzer_ausgewaehlt", benutzer_ausgewaehlt);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		if (savedInstanceState != null) {
			benutzer = savedInstanceState.containsKey("benutzer") ? (MeineDaten) savedInstanceState
					.getSerializable("benutzer") : null;
			benutzer_ausgewaehlt = savedInstanceState.containsKey("benutzer_ausgewaehlt") ? (MeineDaten) savedInstanceState
					.getSerializable("benutzer_ausgewaehlt") : null;

			writeScreen();
		}
	}

	public class Loader extends AsyncTask<Boolean, String, Boolean> {
		ProgressDialog progDailog;
		String toast_message;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDailog = new ProgressDialog(ShowUserManagerActivity.this);
			progDailog.setMessage(getString(R.string.d_laden));
			progDailog.setIndeterminate(false);
			progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDailog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					Loader.this.cancel(true);
					finish();
				}
			});
			progDailog.show();
		}

		protected Boolean doInBackground(Boolean... auchspeichern) {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

			// HTTP Client that supports streaming uploads and downloads
			DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());

			// Define that I want to use the POST method to grab data from
			// the provided URL
			HttpPost httppost = new HttpPost(StartActivity.DB_CONNECTION_URL + "?action=UserManager&email="
					+ benutzer.getEmail().toLowerCase() + "&pass=" + benutzer.getPasswort());

			// Bill speichern sofern schon einer vorhanden ist sonst laden

			if (auchspeichern.length > 0) {
				List<NameValuePair> pairs = new ArrayList<NameValuePair>();
				if (benutzer_ausgewaehlt != null && auchspeichern[0] && !benutzer_ausgewaehlt.isLoeschen()) {
					pairs.add(new BasicNameValuePair("u_name", String.valueOf(benutzer_ausgewaehlt.getName())));
					pairs.add(new BasicNameValuePair("u_id", String.valueOf(benutzer_ausgewaehlt.getId())));
					pairs.add(new BasicNameValuePair("u_email", String.valueOf(benutzer_ausgewaehlt.getEmail())));

				}
				if (benutzer_ausgewaehlt.isLoeschen()) {
					pairs.add(new BasicNameValuePair("u_loeschen", String.valueOf(benutzer_ausgewaehlt.getId())));
				}
				try {
					httppost.setEntity(new UrlEncodedFormEntity(pairs));
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			// Web service used is defined
			// httppost.setHeader("Content-type", "application/json");
			httppost.setHeader("Content-type", "application/x-www-form-urlencoded");

			// Used to read data from the URL
			InputStream inputStream = null;

			// Will hold the whole all the data gathered from the URL
			String result = null;

			try {

				// Get a response if any from the web service
				HttpResponse response = httpclient.execute(httppost);

				// The content from the requested URL along with headers, etc.
				HttpEntity entity = response.getEntity();

				// Get the main content from the URL
				inputStream = entity.getContent();

				// JSON is UTF-8 by default
				// BufferedReader reads data from the InputStream until the
				// Buffer is full
				BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);

				// Will store the data
				StringBuilder theStringBuilder = new StringBuilder();

				String line = null;

				// Read in the data from the Buffer untilnothing is left
				while ((line = reader.readLine()) != null) {

					// Add data from the buffer to the StringBuilder
					theStringBuilder.append(line + "\n");
				}

				// Store the complete data in result
				result = theStringBuilder.toString();

			} catch (Exception e) {
				e.printStackTrace();
				return false;
			} finally {

				// Close the InputStream when you're done with it
				try {
					if (inputStream != null)
						inputStream.close();
				} catch (Exception e) {
				}
			}
			// Print out all the data read in
			// Log.v("JSONParser RESULT ", result);

			// Holds Key Value pairs from a JSON source
			JSONObject jsonObject;
			try {
				// Get the root JSONObject
				jsonObject = new JSONObject(result);

				if (!jsonObject.getJSONArray("userdata").isNull(0)) {
					// Benutzer laden
					JSONObject jsonuser = jsonObject.getJSONArray("userdata").getJSONObject(0);
					benutzer.setName(jsonuser.getString("name"));
					benutzer.setId(jsonuser.getInt("id"));

					// andere Benutzer die eigene Kinder sind
					JSONArray JSONusermanageruserdata = jsonObject.getJSONArray("usermanageruserdata");
					for (int i = 0; i < JSONusermanageruserdata.length(); i++) {
						benutzer.putKind(new Benutzer(
								JSONusermanageruserdata.getJSONObject(i)));
					}
					// Fehler feststellen
					if (!jsonObject.isNull("error_emailalreadyinuse")) {
						toast_message = getString(R.string.t_benutzerschonangemeldet);
						setResult(Activity.RESULT_OK,
								new Intent().putExtra("selected_user_id", jsonObject.getJSONArray("error_emailalreadyinuse").getJSONObject(0).getInt("id")));
						finish();
					}
					if (benutzer_ausgewaehlt.isLoeschen()) {
						if (!jsonObject.isNull("error_userstillinuse")) {
							toast_message = getString(R.string.t_benutzerwirdnochgebraucht);
							benutzer_ausgewaehlt.setLoeschen(false);
						} else
							toast_message = getString(R.string.t_geloescht);
					}
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return true;

		}

		protected void onPostExecute(Boolean result) {
			if (progDailog.isShowing()) {
				progDailog.dismiss();
			}
			if (!result) {
				startActivity((new Intent(getBaseContext(), StartActivity.class))
						.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				return;
			}
			if (benutzer.getId() > 0) {
				benutzer.setSync(true);

				writeScreen();

				// Fehlermeldung anzeigen
				if (toast_message != null)
					Toast.makeText(getApplicationContext(), toast_message, Toast.LENGTH_LONG).show();

			} else {
				Intent intent = new Intent(getBaseContext(), StartActivity.class);
				startActivity(intent);
			}
		}
	}

	private boolean showError(EditText ETreferenz, String fehlertext) {
		Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
		ETreferenz.startAnimation(shake);
		ETreferenz.setError(fehlertext);
		ETreferenz.requestFocus();
		return true;
	}

	public static class ListAdapter extends BaseAdapter {

		private final Activity activity;
		private static LayoutInflater inflater = null;
		private ArrayList<Integer> keyset;

		public ListAdapter(Activity a) {
			activity = a;
			inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			keyset = benutzer.getKinderKeysByPosition();
		}

		public int keyAt(int position) {
			return keyset.get(position);
		}

		@Override
		public boolean areAllItemsEnabled() {
			return true;
		}

		@Override
		public boolean isEnabled(int arg0) {
			return true;
		}

		@Override
		public int getCount() {
			return benutzer.sizeOfKinder();
		}

		@Override
		public Object getItem(int position) {
			return keyAt(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			int user_id = keyAt(position);
			View vi = inflater.inflate(R.layout.list_user_manager_row, null);

			((TextView) vi.findViewById(R.id.ShowUserManagerListRowName)).setText(benutzer.getKind(user_id).getName());
			((TextView) vi.findViewById(R.id.ShowUserManagerListRowEmail))
					.setText(benutzer.getKind(user_id).getEmail());
			return vi;
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.
	}
}
