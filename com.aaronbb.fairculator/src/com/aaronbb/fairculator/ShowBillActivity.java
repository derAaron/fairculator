package com.aaronbb.fairculator;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.Format;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aaronbb.fairculator.ShowBillEditActivity.Loader;
import com.aaronbb.fairculator.daten.Benutzer;
import com.aaronbb.fairculator.daten.HttpAnfrage;
import com.aaronbb.fairculator.daten.MeineDaten;
import com.aaronbb.fairculator.daten.Bill;
import com.aaronbb.fairculator.daten.Event;
import com.aaronbb.fairculator.daten.Bill.Eigenanteil;
import com.aaronbb.fairculator.extra.RoundedImageView;
import com.google.analytics.tracking.android.EasyTracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class ShowBillActivity extends FairculatorActivity {
	// public MeinDaten benutzer;
	// private SharedPreferences sharedprefs;
	private int bill_id, event_id;
	static Bill bill;
	static Event aktuellesevent;

	// private static Map<Integer, String> couser = new HashMap<Integer,
	// String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Benutzeremail und Passwort laden
		// sharedprefs = getSharedPreferences("fairculator", MODE_PRIVATE);
		// benutzer = new MeinDaten(sharedprefs.getString("email", ""),
		// sharedprefs.getString("pass",""));

		// Bill ID laden
		bill_id = sharedprefs.getInt("bill_id", 0);
		event_id = sharedprefs.getInt("event_id", 0);

		new Loader().execute();
		// Show the Up button in the action bar.
		setupActionBar();
	}

	@Override
	protected void onResume() {
		super.onResume();
		new Loader().execute();
	}

	public void onCreateView() {

		if (aktuellesevent == null)
			aktuellesevent = benutzer.getEvent(event_id);

		// Wenn der Bill noch nicht geladen ist
		if (bill == null)
			bill = benutzer.getEvent(event_id).getBill(bill_id);

			setContentView(R.layout.activity_show_bill);

			((TextView) findViewById(R.id.ShowBillShop)).setText(bill.getShop());
			((TextView) findViewById(R.id.ShowBillText)).setText(bill.getText());
			((TextView) findViewById(R.id.ShowBillTotalE))
					.setText(String.valueOf(GeldFormat(aktuellesevent.getLocale()).format(bill.getTotal_betrag())));
			Format dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
			((TextView) findViewById(R.id.ShowBillDatum)).setText(dateFormat.format(bill.getDatum()));
			((TextView) findViewById(R.id.ShowBillEditZahler))
					.setText(benutzer.getCouser(bill.getOwner_id()).getName());
			RoundedImageView ShowBillThumb = (RoundedImageView) findViewById(R.id.ShowBillPicThumb);

			ListAdapter adapter = new ListAdapter(ShowBillActivity.this);
			ListView list = (ListView) findViewById(R.id.ShowBillAufteilung);
			list.setAdapter(adapter);

			if (bill.getFotoChecksum() != null) {
				ShowBillThumb.setBackgroundDrawable(null);
				ImageLoader.getInstance().displayImage(
						LOAD_BILL_PIC_URL + "?email=" + benutzer.getEmail() + "&pass=" + benutzer.getPasswort()
								+ "&action=loadbill&bill_id=" + bill_id + "&thumb&cs=" + bill.getFotoChecksum(),
						ShowBillThumb,
						new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.ic_action_warning)
								.showImageOnFail(R.drawable.ic_action_error).cacheInMemory(true).cacheOnDisc(true)
								.considerExifParams(true).imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
								.imageScaleType(ImageScaleType.EXACTLY_STRETCHED).build(),
						new SimpleImageLoadingListener() {

							@Override
							public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
								String message = null;
								switch (failReason.getType()) {
									case IO_ERROR:
										message = "Input/Output error";
										break;
									case DECODING_ERROR:
										message = "Image can't be decoded";
										break;
									case NETWORK_DENIED:
										message = "Downloads are denied";
										break;
									case OUT_OF_MEMORY:
										message = "Out Of Memory error";
										break;
									case UNKNOWN:
										message = "Unknown error";
										break;
								}
								Toast.makeText(ShowBillActivity.this, message, Toast.LENGTH_SHORT).show();
							}
						});

				ShowBillThumb.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						startActivity(new Intent(getBaseContext(), ShowBillPictureActivity.class).putExtra(
								"foto_checksum", bill.getFotoChecksum()));
					}
				});
			} else {
				ShowBillThumb.setVisibility(View.GONE);
				findViewById(R.id.ShowBillPicText).setVisibility(View.GONE);
			}
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	// @Override
	// protected void onResume() {
	// // TODO Auto-generated method stub
	// super.onResume();
	//
	// if(benutzer.hasEvent(event_id)) aktuellesevent =
	// benutzer.getEvent(event_id);
	// }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.show_bill, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				// This ID represents the Home or Up button. In the case of this
				// activity, the Up button is shown. Use NavUtils to allow users
				// to navigate up one level in the application structure. For
				// more details, see the Navigation pattern on Android Design:
				//
				// http://developer.android.com/design/patterns/navigation.html#up-vs-back
				//
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.show_bill_abmelden:
				SharedPreferences.Editor prefEditor = sharedprefs.edit();
				prefEditor.remove("email");
				prefEditor.remove("pass");
				prefEditor.commit();
				benutzer.abmelden();
				Intent i = new Intent(getBaseContext(), StartActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public class Loader extends AsyncTask<String, String, Boolean> {
		ProgressDialog progDailog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDailog = new ProgressDialog(ShowBillActivity.this);
			progDailog.setMessage(getString(R.string.d_laden));
			progDailog.setIndeterminate(false);
			progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDailog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					Loader.this.cancel(true);
					finish();
				}
			});
			progDailog.show();
		}

		protected Boolean doInBackground(String... arg0) {
			if (bill == null)
				bill = benutzer.getEvent(event_id).getBill(bill_id);

			if (handleSyncRequest(bill)) {
				if (bill.getId() > 0)
					bill.update(benutzer.getEvent(bill.getEvent_id()).getBill(bill.getId()));
				// bill_kopie.update(benutzer.getEvent(event_id).getBill(bill_id));
				return true;
			} else
				return false;
/*
			String result = executeRequest(new HttpAnfrage(DB_CONNECTION_URL + "?action=loadbill&bill_id=" + bill_id
					+ "&email=" + benutzer.getEmail().toLowerCase() + "&pass=" + benutzer.getPasswort()));
			if (result == null)
				return false;

			// Holds Key Value pairs from a JSON source
			JSONObject jsonObject;
			try {
				// Get the root JSONObject
				jsonObject = new JSONObject(result);

				if (!jsonObject.getJSONArray("userdata").isNull(0)) {
					// Benutzer laden
					benutzer.update(new MeineDaten(jsonObject.getJSONArray("userdata").getJSONObject(0)));
					benutzer.setSync(benutzer.getId() > 0);

					// Event laden
					if (!jsonObject.isNull("eventsdata")) {
						aktuellesevent = new Event(jsonObject.getJSONArray("eventsdata").getJSONObject(0));
						if (benutzer.hasEvent(aktuellesevent.getId())) {
							benutzer.getEvent(aktuellesevent.getId()).update(aktuellesevent);
							aktuellesevent = benutzer.getEvent(aktuellesevent.getId());
						} else
							benutzer.putEvent(aktuellesevent);
					}

					// Bills laden
					if (!jsonObject.isNull("billdata")) {
						bill = new Bill(jsonObject.getJSONArray("billdata").getJSONObject(0), aktuellesevent);
						aktuellesevent.putBill(bill);
					}

					// andere Usernamen laden damit diese den ids zugeordnet
					// werden k�nnen
					JSONArray JSONcousers = jsonObject.getJSONArray("cousersdata");
					for (int i = 0; i < JSONcousers.length(); i++) {
						Benutzer couser = new Benutzer(JSONcousers.getJSONObject(i));

						// Wenn der Benutzer bereits lokal gespeichert ist,
						// ihn nicht neu erzeugen sondern seine Daten updaten
						// und ihn verwenden
						benutzer.putCouser(couser);
						aktuellesevent.putCouser(benutzer.getCouser(couser.getId()));
					}

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;*/
		}

		protected void onPostExecute(Boolean result) {
			if (progDailog.isShowing()) {
				progDailog.dismiss();
			}
			if (!result && (!benutzer.hasEvent(event_id) || !benutzer.getEvent(event_id).hasBill(bill_id))) {
				startActivity((new Intent(getBaseContext(), StartActivity.class))
						.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				return;
			}
			
			if (benutzer.isVerified()) {
				onCreateView();
			} else {
				Intent intent = new Intent(getBaseContext(), StartActivity.class);
				startActivity(intent);
			}
/*
			aktuellesevent = benutzer.getEvent(event_id);
			bill = benutzer.getEvent(event_id).getBill(bill_id);

			if (benutzer.isVerified()) {

				setContentView(R.layout.activity_show_bill);

				((TextView) findViewById(R.id.ShowBillShop)).setText(bill.getShop());
				((TextView) findViewById(R.id.ShowBillText)).setText(bill.getText());
				((TextView) findViewById(R.id.ShowBillTotalE)).setText(String.valueOf(GeldFormat(
						aktuellesevent.getLocale()).format(bill.getTotal_betrag())));
				Format dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
				((TextView) findViewById(R.id.ShowBillDatum)).setText(dateFormat.format(bill.getDatum()));
				((TextView) findViewById(R.id.ShowBillEditZahler)).setText(benutzer.getCouser(bill.getOwner_id())
						.getName());
				RoundedImageView ShowBillThumb = (RoundedImageView) findViewById(R.id.ShowBillPicThumb);

				ListAdapter adapter = new ListAdapter(ShowBillActivity.this);
				ListView list = (ListView) findViewById(R.id.ShowBillAufteilung);
				list.setAdapter(adapter);

				if (bill.getFotoChecksum() != null) {
					ShowBillThumb.setBackgroundDrawable(null);
					ImageLoader.getInstance().displayImage(
							LOAD_BILL_PIC_URL + "?email=" + benutzer.getEmail() + "&pass=" + benutzer.getPasswort()
									+ "&action=loadbill&bill_id=" + bill_id + "&thumb&cs=" + bill.getFotoChecksum(),
							ShowBillThumb,
							new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.ic_action_warning)
									.showImageOnFail(R.drawable.ic_action_error).cacheInMemory(true).cacheOnDisc(true)
									.considerExifParams(true).imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
									.imageScaleType(ImageScaleType.EXACTLY_STRETCHED).build(),
							new SimpleImageLoadingListener() {

								@Override
								public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
									String message = null;
									switch (failReason.getType()) {
										case IO_ERROR:
											message = "Input/Output error";
											break;
										case DECODING_ERROR:
											message = "Image can't be decoded";
											break;
										case NETWORK_DENIED:
											message = "Downloads are denied";
											break;
										case OUT_OF_MEMORY:
											message = "Out Of Memory error";
											break;
										case UNKNOWN:
											message = "Unknown error";
											break;
									}
									Toast.makeText(ShowBillActivity.this, message, Toast.LENGTH_SHORT).show();
								}
							});

					ShowBillThumb.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							startActivity(new Intent(getBaseContext(), ShowBillPictureActivity.class).putExtra(
									"foto_checksum", bill.getFotoChecksum()));
						}
					});
				} else {
					ShowBillThumb.setVisibility(View.GONE);
					findViewById(R.id.ShowBillPicText).setVisibility(View.GONE);
				}
			} else {
				Intent intent = new Intent(getBaseContext(), StartActivity.class);
				startActivity(intent);
			}*/

		}

	}

	public static class ListAdapter extends BaseAdapter {

		private final Activity activity;
		private static LayoutInflater inflater = null;
		private ArrayList<Integer> keyset;

		public ListAdapter(Activity a) {
			activity = a;
			inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			keyset = bill.getUserKeysByPosition();
		}

		public int keyAt(int position) {
			return keyset.get(position);
		}

		@Override
		public boolean areAllItemsEnabled() {
			return true;
		}

		@Override
		public boolean isEnabled(int arg0) {
			return true;
		}

		@Override
		public int getCount() {
			return bill.sizeOfUserInklEigenanteile();
		}

		@Override
		public Object getItem(int position) {
			return keyAt(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			int user_id = keyAt(position);
			View vi = convertView;

			// if(convertView==null)
			vi = inflater.inflate(R.layout.list_betraege_row, null);

			TextView TVname = (TextView) vi.findViewById(R.id.show_bill_list_user);

			TVname.setText(benutzer.getCouser(user_id).getName());

			TextView TVeigenanteil = (TextView) vi.findViewById(R.id.ShowBillListEigenanteil);
			Double eigenanteil = bill.getEigenanteilById(user_id);
			Double eigenpfand = bill.getEigenPfandanteilById(user_id);
			if ((eigenanteil != null && eigenanteil != 0.0) || (eigenpfand != null && eigenpfand != 0.0)) {
				String text = "(";
				if (eigenpfand != null && eigenpfand != 0.0)
					text += "<font color=\"" + String.format("#%06X", (0xFFFFFF & FARBE_WALD_GRUEN)) + "\">"
							+ GeldFormat(aktuellesevent.getLocale()).format(eigenpfand) + "</font>";
				if ((eigenanteil != null && eigenanteil != 0.0) && (eigenpfand != null && eigenpfand != 0.0))
					text += " / ";
				if (eigenanteil != null && eigenanteil != 0.0)
					text += "<font color=\"" + String.format("#%06X", (0xFFFFFF & FARBE_WALD_ROT)) + "\">"
							+ GeldFormat(aktuellesevent.getLocale()).format(eigenanteil) + "</font>";
				text += ")";
				TVeigenanteil.setText(Html.fromHtml(text));
			}
			((TextView) vi.findViewById(R.id.show_bill_list_beitrag)).setText(String.valueOf(GeldFormat(
					aktuellesevent.getLocale()).format(bill.getAnteilbyId(user_id))));
			return vi;
		}
	}

}
