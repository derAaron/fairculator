package com.aaronbb.fairculator;

import java.text.DecimalFormatSymbols;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.aaronbb.fairculator.daten.Bill;
import com.aaronbb.fairculator.extra.MoneyKeyboard;

public class ShowBillEditEigenanteilActivity extends Activity {
	private MoneyKeyboard mCustomKeyboard;
	private EditText ET_edit_eigenanteil, ET_edit_eigenpfand;
	private Bill bill;
	private Locale locale;
	private int selected_user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState == null) {
			Bundle extras = getIntent().getExtras();
			if (extras == null) {
				bill = null;
				locale = null;
				selected_user = 0;
			} else {
				bill = (Bill) extras.get("bill");
				locale = (Locale) extras.get("locale");
				selected_user = (int) extras.get("selected_user");
			}
			writeScreen();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.show_bill_edit_eigenanteil, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.ShowBillEditEigenanteilOk:
				speichern(true);
				return true;

//			case R.id.ShowBillEditEigenanteilCancel:
//				finish();
//				return true;

		}
		return super.onOptionsItemSelected(item);
	}

	private void writeScreen() {

		setContentView(R.layout.activity_show_bill_edit_eigenanteil);
		ET_edit_eigenanteil = (EditText) findViewById(R.id.ShowBillEditEigenanteil);
		ET_edit_eigenpfand = (EditText) findViewById(R.id.ShowBillEditEigenpfand);

		// stattdessen den Dialog anzeigen
		mCustomKeyboard = new MoneyKeyboard(this, R.id.keyboardview2, R.xml.money_keyboard,
				String.valueOf((new DecimalFormatSymbols(locale)).getDecimalSeparator()));
		mCustomKeyboard.registerEditText(R.id.ShowBillEditEigenanteil);
		mCustomKeyboard.registerEditText(R.id.ShowBillEditEigenpfand);

		ET_edit_eigenanteil.setText(StartActivity.GeldFormatOhneE(locale)
				.format(bill.getEigenanteilById(selected_user)));
		ET_edit_eigenanteil.setTextColor(StartActivity.FARBE_WALD_ROT);
		ET_edit_eigenpfand.setText(StartActivity.GeldFormatOhneE(locale).format(
				bill.getEigenPfandanteilById(selected_user)));
		ET_edit_eigenpfand.setTextColor(StartActivity.FARBE_WALD_GRUEN);
		ET_edit_eigenanteil.selectAll();
		ET_edit_eigenpfand.setOnEditorActionListener(new OnEditorActionListener() {

			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEND) {
					speichern(true);
					return true;
				}
				return false;
			}
		});
		ET_edit_eigenanteil.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				speichern(false);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}
		});
		ET_edit_eigenpfand.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				speichern(false);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}
		});

	}

	protected void speichern(boolean zurrueck) {
		Boolean fehlergefunden = false;
		Double betrag = StartActivity.parseMoney(ET_edit_eigenanteil.getText().toString(), locale);
		Double pfand = StartActivity.parseMoney(ET_edit_eigenpfand.getText().toString(), locale);

		if (betrag == null || betrag < 0.0) {
			fehlergefunden = showError(ET_edit_eigenanteil, getString(R.string.e_keingeldbetrag));
		} else
			ET_edit_eigenanteil.setError(null);
		if (pfand == null || pfand < 0.0) {
			fehlergefunden = showError(ET_edit_eigenpfand, getString(R.string.e_keingeldbetrag));
		} else
			ET_edit_eigenpfand.setError(null);

		if (!fehlergefunden) {
			bill.putUserInklEigenanteil(bill.getCouser(selected_user), betrag, pfand);
			if (zurrueck) {
				if (getParent() == null) {
					setResult(Activity.RESULT_OK, (new Intent()).putExtra("bill", bill));
				} else {
					getParent().setResult(Activity.RESULT_OK, (new Intent()).putExtra("bill", bill));
				}
				finish();
			}
		}
	}

	private boolean showError(EditText ETreferenz, String fehlertext) {
		Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
		ETreferenz.startAnimation(shake);
		ETreferenz.setError(fehlertext);
		ETreferenz.requestFocus();
		return true;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (bill != null)
			outState.putSerializable("bill", bill);
		if (locale != null)
			outState.putSerializable("locale", locale);
		if (selected_user != 0)
			outState.putInt("selected_user", selected_user);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		if (savedInstanceState != null) {
			bill = savedInstanceState.containsKey("bill") ? (Bill) savedInstanceState.getSerializable("bill") : null;
			locale = savedInstanceState.containsKey("locale") ? (Locale) savedInstanceState.getSerializable("locale")
					: null;
			selected_user = savedInstanceState.containsKey("selected_user") ? (int) savedInstanceState
					.getInt("selected_user") : 0;

			writeScreen();
		}
	}

}
