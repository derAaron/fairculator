package com.aaronbb.fairculator;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.aaronbb.fairculator.daten.Benutzer;
import com.aaronbb.fairculator.daten.Bill;
import com.aaronbb.fairculator.daten.Event;
import com.aaronbb.fairculator.daten.HttpAnfrage;
import com.aaronbb.fairculator.daten.MeineDaten;
import com.google.analytics.tracking.android.EasyTracker;

public class ShowEventActivity extends FairculatorActivity {
	private Menu menu;
	private int event_id;
	ListView list;
	private int delete_bill = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Event ID laden
		event_id = sharedprefs.getInt("event_id", 0);

		// Show the Up button in the action bar.
		setupActionBar();

		if (savedInstanceState == null)
			new Loader().execute();
	}

	@Override
	public void onCreateView() {
		super.onCreateView();

		final Event aktuellesevent = benutzer.getEvent(event_id);
		toast("Menge: " +benutzer.getEvent(event_id).getPositionKeyTable().toString());

		setContentView(R.layout.activity_show_event);

		list = (ListView) findViewById(R.id.list_bills);

		try {
			if (aktuellesevent.getOwner() == benutzer.getId())
				menu.findItem(R.id.edit_event).setVisible(true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (aktuellesevent.isLoaded()) {
			setTitle(aktuellesevent.getTitel());

			NumberFormat waehrungsformat = NumberFormat.getCurrencyInstance(aktuellesevent.getLocale());
			waehrungsformat.setCurrency(Currency.getInstance("EUR"));
			waehrungsformat.setMaximumFractionDigits(Currency.getInstance("EUR").getDefaultFractionDigits());
			waehrungsformat.setMinimumFractionDigits(Currency.getInstance("EUR").getDefaultFractionDigits());

			// Getting adapter by passing xml data ArrayList
			list.setAdapter(new ListAdapter(ShowEventActivity.this, aktuellesevent));
			list.setClickable(true);
			list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {

					Intent intent; // Wenn Benutzer auch Owner des Bills
									// oder der Rechnung ist, wird der
									// Editor ge�ffnet
					if (aktuellesevent.getBill((Integer) parent.getAdapter().getItem(position)).getOwner_id() == benutzer
							.getId() || aktuellesevent.getOwner() == benutzer.getId()) {
						intent = new Intent(getBaseContext(), ShowBillEditActivity.class);
					} else {
						intent = new Intent(getBaseContext(), ShowBillActivity.class);
					}
					sharedprefs.edit().putInt("bill_id", (Integer) parent.getAdapter().getItem(position)).commit();
					startActivity(intent);
				}

			});
			list.setOnItemLongClickListener(new OnItemLongClickListener() {

				@Override
				public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long arg3) {
					if (aktuellesevent.getBill((Integer) parent.getAdapter().getItem(position)).getOwner_id() == benutzer
							.getId() || benutzer.getId() == aktuellesevent.getOwner())
						bill_loeschen((Integer) parent.getAdapter().getItem(position));
					return true;
				}
			});
			String Guthabensatz = getString(R.string.text_event_guthaben);
			String Schuldensatz = getString(R.string.text_event_schulden);
			String KommaG = "";
			String KommaS = "";
			for (Entry<Integer, Double> e : aktuellesevent.getGuthabenBei(benutzer.getId()).entrySet()) {
				if (e.getValue() > 0.005) {
					Guthabensatz = Guthabensatz + KommaG + " " + "<b>"
							+ GeldFormat(aktuellesevent.getLocale()).format(e.getValue()) + "</b> "
							+ getString(R.string.text_event_bei) + " "
							+ aktuellesevent.getCouser((e.getKey())).getName();
					KommaG = ",";
				}
				if (e.getValue() < -0.005) {
					Schuldensatz = Schuldensatz + KommaS + " " + "<b>"
							+ GeldFormat(aktuellesevent.getLocale()).format(e.getValue() * -1) + "</b> "
							+ getString(R.string.text_event_bei) + " "
							+ aktuellesevent.getCouser((e.getKey())).getName();
					KommaS = ",";
				}
			}
			if (Guthabensatz != getString(R.string.text_event_guthaben)
					&& Schuldensatz != getString(R.string.text_event_schulden))
				((TextView) findViewById(R.id.ShowBillUntermStrich)).setText(Html.fromHtml(Guthabensatz + "<br>"
						+ Schuldensatz));
			if (Guthabensatz != getString(R.string.text_event_guthaben)
					&& Schuldensatz == getString(R.string.text_event_schulden))
				((TextView) findViewById(R.id.ShowBillUntermStrich)).setText(Html.fromHtml(Guthabensatz));
			if (Guthabensatz == getString(R.string.text_event_guthaben)
					&& Schuldensatz != getString(R.string.text_event_schulden))
				((TextView) findViewById(R.id.ShowBillUntermStrich)).setText(Html.fromHtml(Schuldensatz));
		}

	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		this.menu = menu;
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.show_event, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;

		switch (item.getItemId()) {
			case android.R.id.home:
				// This ID represents the Home or Up button. In the case of this
				// activity, the Up button is shown. Use NavUtils to allow users
				// to navigate up one level in the application structure. For
				// more details, see the Navigation pattern on Android Design:
				//
				// http://developer.android.com/design/patterns/navigation.html#up-vs-back
				//
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.edit_event:
				intent = new Intent(getBaseContext(), ShowEventEditActivity.class);
				startActivity(intent);
				return true;

			case R.id.EventNewBill:
				intent = new Intent(getBaseContext(), ShowBillEditActivity.class);
				sharedprefs.edit().putInt("bill_id", 0).commit();
				startActivity(intent);
				return true;

			case R.id.action_settings:
				startActivity(new Intent(getBaseContext(), ShowSettingsActivity.class));
				return true;

			case R.id.show_event_abmelden:
				SharedPreferences.Editor prefEditor = sharedprefs.edit();
				prefEditor.remove("email");
				prefEditor.remove("pass");
				prefEditor.commit();
				benutzer.abmelden();
				Intent i = new Intent(getBaseContext(), StartActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public class Loader extends AsyncTask<String, String, Boolean> {
		ProgressDialog progDailog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDailog = new ProgressDialog(ShowEventActivity.this);
			progDailog.setMessage(getString(R.string.d_laden));
			progDailog.setIndeterminate(false);
			progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDailog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					Loader.this.cancel(true);
					finish();
				}
			});
			progDailog.show();
		}

		protected Boolean doInBackground(String... arg0) {
			Event aktuellesevent;
			HashMap<String, String> pairs = null;

			// Soll ein Bill gel�scht werden?
			if (delete_bill > 0) {
				pairs = new HashMap<String, String>();
				pairs.put("delete_bill", String.valueOf(delete_bill));

			}

			String result = executeRequest(new HttpAnfrage(DB_CONNECTION_URL + "?action=loadbills&event_id=" + event_id
					+ "&email=" + benutzer.getEmail().toLowerCase() + "&pass=" + benutzer.getPasswort(), pairs));
			if (result == null)
				return false;
			// Holds Key Value pairs from a JSON source
			JSONObject jsonObject;
			try {
				// Get the root JSONObject
				jsonObject = new JSONObject(result);

				if (!jsonObject.getJSONArray("userdata").isNull(0)) {
					// Benutzer laden
					benutzer.update(new MeineDaten(jsonObject.getJSONArray("userdata").getJSONObject(0)));
					benutzer.setSync(benutzer.getId() > 0);

					// Eventdaten laden
					aktuellesevent = new Event(jsonObject.getJSONArray("eventsdata").getJSONObject(0));
					if (benutzer.hasEvent(aktuellesevent.getId())) {
						benutzer.getEvent(aktuellesevent.getId()).update(aktuellesevent);
						aktuellesevent = benutzer.getEvent(aktuellesevent.getId());
					} else
						benutzer.putEvent(aktuellesevent);

					JSONArray JSONcousers = jsonObject.getJSONArray("cousersdata");
					aktuellesevent.clearCouser();
					for (int i = 0; i < JSONcousers.length(); i++) {
						Benutzer couser = new Benutzer(JSONcousers.getJSONObject(i));

						// Wenn der Benutzer bereits lokal gespeichert ist, ihn
						// nicht neu erzeugen sondern seine Daten updaten und
						// ihn verwenden
						benutzer.putCouser(couser);
						aktuellesevent.putCouser(benutzer.getCouser(couser.getId()));
					}

					// Bills laden
					JSONArray JSONbills = jsonObject.getJSONArray("billsdata");
					for (int i = 0; i < JSONbills.length(); i++) {
						Bill bill = new Bill(JSONbills.getJSONObject(i), aktuellesevent);

						bill.setLoaded(true);
						aktuellesevent.putBill(bill);
					}
					aktuellesevent.setSync(true);
					benutzer.putEvent(aktuellesevent);

				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return true;

		}

		protected void onPostExecute(Boolean result) {
			if (progDailog.isShowing()) {
				progDailog.dismiss();
			}
			if (!result && !benutzer.hasEvent(event_id)) {
				startActivity((new Intent(getBaseContext(), StartActivity.class))
						.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				return;
			}
			Log.v("test", "" + benutzer.getEvent(event_id).getBills().size());

			if (benutzer.isVerified() && benutzer.hasEvent(event_id)) {
				onCreateView();
			} else {
				// Wenn Benutzer nicht verifiziert ist dann zur�ck
				Intent intent = new Intent(getBaseContext(), StartActivity.class);
				startActivity(intent);
			}

		}

	}

	private void bill_loeschen(final Integer bill_id) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle(getString(R.string.d_loeschen));
		builder.setMessage(getString(R.string.d_sicher));

		builder.setPositiveButton(getString(R.string.d_ja), new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				delete_bill = bill_id;
				list.removeAllViewsInLayout();
				benutzer.getEvent(event_id).delBill(delete_bill);
				new Loader().execute();
			}

		});

		builder.setNegativeButton(getString(R.string.d_nein), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				delete_bill = 0;
			}
		});

		AlertDialog alert = builder.create();
		alert.show();
	}

	public class ListAdapter extends BaseAdapter {

		private LayoutInflater inflater = null;
		private ArrayList<Integer> keyTable;
		private Event aktuellesevent;

		public ListAdapter(Activity a, Event aktuellesevent) {
			this.aktuellesevent = aktuellesevent;
			inflater = (LayoutInflater) a.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			keyTable = this.aktuellesevent.getPositionKeyTable();
		}

		public int keyAt(int position) {
			return keyTable.get(position);
		}

		@Override
		public boolean areAllItemsEnabled() {
			return true;
		}

		@Override
		public boolean isEnabled(int arg0) {
			return true;
		}

		@Override
		public int getCount() {
			return this.aktuellesevent.getAnzahlAnRechungen();
		}

		@Override
		public Object getItem(int position) {
			return keyAt(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			Bill bill = aktuellesevent.getBill(keyAt(position));

			View vi = convertView;
			if (convertView == null)
				vi = inflater.inflate(R.layout.list_bills_row, null);

			TextView bill_date = (TextView) vi.findViewById(R.id.bill_date);
			TextView bill_title = (TextView) vi.findViewById(R.id.bill_title);
			TextView bill_subtitle = (TextView) vi.findViewById(R.id.bill_subtitle);
			TextView bill_price = (TextView) vi.findViewById(R.id.bill_price);

			// Setting all values in listview
			bill_date.setText(dateFormat.format(bill.getDatum()));
			if (!einstellungen.getBoolean("event_datumanzeigen", true))
				bill_date.setVisibility(View.GONE);
			bill_title.setText(bill.getShop() + bill.getId());
			bill_subtitle.setText(bill.getText() + " " + getString(R.string.text_bezahltvon) + " "
					+ aktuellesevent.getCouser(bill.getOwner_id()).getName());
			Double guthabenAnBill = bill.getGuthabenAnBill(benutzer.getId());
			if (guthabenAnBill >= 0) {
				bill_price.setTextColor(FARBE_WALD_GRUEN);
			} else
				bill_price.setTextColor(FARBE_WALD_ROT);
			bill_price.setText(String.valueOf(GeldFormat(aktuellesevent.getLocale()).format(guthabenAnBill)));

			return vi;
		}
	}
}
