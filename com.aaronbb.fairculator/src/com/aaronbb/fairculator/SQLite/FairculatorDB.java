package com.aaronbb.fairculator.SQLite;

import android.provider.BaseColumns;

public class FairculatorDB {
	public static final String TYPE_INTEGER = " INTEGER";
	public static final String TYPE_REAL = " REAL";
	public static final String TYPE_TEXT = " TEXT";
	public static final String TYPE_BLOB = " BLOB";
	public static final String TYPE_DATETIME = " DATETIME";
	public static final String CONSTRAINT_PRIMARY_KEY = " PRIMARY KEY";
	public static final String CONSTRAINT_NOT_NULL = " NOT NULL";
	public static final String AUTOINCREMENT = " AUTOINCREMENT";
	public static final String DEFAULT_CURRENT_TIMESTAMP = " DEFAULT CURRENT_TIMESTAMP";
	public static final String DEFAULT_NULL = " DEFAULT 0.0";
	private static final String COMMA = ", ";


	public FairculatorDB() {}
	

    /* Inner classes that defines the table contents */
    public static abstract class Users implements BaseColumns {
        public static final String TABLE_NAME = "users";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_UPDATED = "updated";
        public static final String COLUMN_STATUS = "status";
        public static final String COLUMN_OWNER = "owner";
        public static final String COLUMN_EMAIL = "email";
        public static final String COLUMN_PASS = "pass";
        public static final String COLUMN_DATUM = "datum";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_EMAIL_NEW = "email_new";
        
        public static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_ID + TYPE_INTEGER + CONSTRAINT_PRIMARY_KEY + AUTOINCREMENT + COMMA +
                COLUMN_UPDATED + TYPE_DATETIME + DEFAULT_CURRENT_TIMESTAMP + COMMA +
                COLUMN_STATUS + TYPE_TEXT + COMMA +
                COLUMN_OWNER + TYPE_INTEGER + COMMA +
                COLUMN_EMAIL + TYPE_TEXT + CONSTRAINT_NOT_NULL+ COMMA +
                COLUMN_PASS + TYPE_TEXT + CONSTRAINT_NOT_NULL+ COMMA +
                COLUMN_DATUM + TYPE_TEXT + CONSTRAINT_NOT_NULL+ COMMA +
                COLUMN_NAME + TYPE_TEXT + CONSTRAINT_NOT_NULL+ COMMA +
                COLUMN_EMAIL_NEW + TYPE_TEXT + CONSTRAINT_NOT_NULL +
                ");";

         public static final String SQL_DROP_TABLE =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
    
    public static abstract class Events implements BaseColumns {
        public static final String TABLE_NAME = "events";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_UPDATED = "updated";
        public static final String COLUMN_STATUS = "status";
        public static final String COLUMN_OWNER = "owner";
        public static final String COLUMN_USERS = "users";
        public static final String COLUMN_TITEL = "titel";
        public static final String COLUMN_TEXT = "text";
        public static final String COLUMN_CURRENCY = "currency";
        
        public static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_ID + TYPE_INTEGER + CONSTRAINT_PRIMARY_KEY + AUTOINCREMENT + COMMA +
                COLUMN_UPDATED + TYPE_DATETIME + DEFAULT_CURRENT_TIMESTAMP + COMMA +
                COLUMN_STATUS + TYPE_TEXT + COMMA +
                COLUMN_OWNER + TYPE_INTEGER + CONSTRAINT_NOT_NULL + COMMA +
                COLUMN_USERS + TYPE_TEXT + CONSTRAINT_NOT_NULL+ COMMA +
                COLUMN_TITEL + TYPE_TEXT + CONSTRAINT_NOT_NULL+ COMMA +
                COLUMN_TEXT + TYPE_TEXT + CONSTRAINT_NOT_NULL+ COMMA +
                COLUMN_CURRENCY + TYPE_TEXT + CONSTRAINT_NOT_NULL +
                ");";

         public static final String SQL_DROP_TABLE =
                "DROP TABLE IF EXISTS " + TABLE_NAME;

    }
    
    public static abstract class Bills implements BaseColumns {
        public static final String TABLE_NAME = "bills";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_UPDATED = "updated";
        public static final String COLUMN_STATUS = "status";
        public static final String COLUMN_EVENT = "event";
        public static final String COLUMN_OWNER = "owner";
        public static final String COLUMN_USERS = "users";
        public static final String COLUMN_BETRAG_AUFTEILUNG = "betrag_aufteilung";
        public static final String COLUMN_PFAND_AUFTEILUNG	 = "pfand_aufteilung";
        public static final String COLUMN_BETRAG = "betrag";
        public static final String COLUMN_SHOP = "shop";
        public static final String COLUMN_DATUM = "datum";
        public static final String COLUMN_TEXT = "text";
        public static final String COLUMN_BILD = "bild";
        
        public static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_ID + TYPE_INTEGER + CONSTRAINT_PRIMARY_KEY + AUTOINCREMENT + COMMA +
                COLUMN_UPDATED + TYPE_DATETIME + DEFAULT_CURRENT_TIMESTAMP + COMMA +
                COLUMN_STATUS + TYPE_TEXT + COMMA +
                COLUMN_EVENT + TYPE_INTEGER + CONSTRAINT_NOT_NULL + COMMA +
                COLUMN_OWNER + TYPE_INTEGER + CONSTRAINT_NOT_NULL + COMMA +
                COLUMN_USERS + TYPE_TEXT + CONSTRAINT_NOT_NULL+ COMMA +
                COLUMN_BETRAG_AUFTEILUNG + TYPE_TEXT + CONSTRAINT_NOT_NULL+ COMMA +
                COLUMN_PFAND_AUFTEILUNG + TYPE_TEXT + CONSTRAINT_NOT_NULL+ COMMA +
                COLUMN_BETRAG + TYPE_REAL + DEFAULT_NULL + COMMA +
                COLUMN_SHOP + TYPE_TEXT + CONSTRAINT_NOT_NULL + COMMA +
                COLUMN_DATUM + TYPE_DATETIME + CONSTRAINT_NOT_NULL + COMMA +
                COLUMN_TEXT + TYPE_TEXT + CONSTRAINT_NOT_NULL+ COMMA +
                COLUMN_BILD + TYPE_BLOB +
                ");";

         public static final String SQL_DROP_TABLE =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
         
    }
}
