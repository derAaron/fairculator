package com.aaronbb.fairculator.SQLite;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class FairculatorDBHelper extends SQLiteOpenHelper {
	
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Fairculator.db";


	public FairculatorDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}


	@Override
	public void onCreate(SQLiteDatabase db) {
        db.execSQL(FairculatorDB.Users.SQL_CREATE_TABLE);
        db.execSQL(FairculatorDB.Events.SQL_CREATE_TABLE);
        db.execSQL(FairculatorDB.Bills.SQL_CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(FairculatorDB.Users.SQL_DROP_TABLE);
        db.execSQL(FairculatorDB.Events.SQL_DROP_TABLE);
        db.execSQL(FairculatorDB.Bills.SQL_DROP_TABLE);
	}

}
