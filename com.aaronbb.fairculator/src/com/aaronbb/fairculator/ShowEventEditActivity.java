package com.aaronbb.fairculator;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v4.app.NavUtils;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.aaronbb.fairculator.daten.Benutzer;
import com.aaronbb.fairculator.daten.Event;
import com.aaronbb.fairculator.daten.MeineDaten;
import com.google.analytics.tracking.android.EasyTracker;

public class ShowEventEditActivity extends Activity {
	public MeineDaten benutzer;
	private SharedPreferences sharedprefs;
	private Menu menu;
	private int event_id;
	private Event aktuellesevent;
	// int delete_user;
	private Boolean delete_event = false;

	// Formularfelder
	private EditText e_titel, e_text;
	private ListView e_userliste;
	private LinearLayout ShowEventEditHeader;
	private int userposition;

	// IDs
	private static final int D_ADMIN_WECHSELN = 1;
	private static final int D_NEUER_USER = 2;
	private static final int D_EVENT_LOESCHEN = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Benutzeremail und Passwort laden
		sharedprefs = getSharedPreferences("fairculator", MODE_PRIVATE);
		benutzer = new MeineDaten(sharedprefs.getString("email", ""), sharedprefs.getString("pass", ""));

		PreferenceManager.getDefaultSharedPreferences(this);

		// Event ID laden
		event_id = sharedprefs.getInt("event_id", 0);

		// Show the Up button in the action bar.
		setupActionBar();
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		new Loader().execute(false);
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		sharedprefs.edit().putInt("event_id", event_id).commit();
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.show_event_edit, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				// This ID represents the Home or Up button. In the case of this
				// activity, the Up button is shown. Use NavUtils to allow users
				// to navigate up one level in the application structure. For
				// more details, see the Navigation pattern on Android Design:
				//
				// http://developer.android.com/design/patterns/navigation.html#up-vs-back
				//
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.show_event_edit_save:
				speichern();
				return true;

			case R.id.show_event_edit_delete:
				showDialog(D_EVENT_LOESCHEN);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu, v, menuInfo);
		if (aktuellesevent.getCouserIdByPosition((int) ((AdapterContextMenuInfo) menuInfo).id) != aktuellesevent
				.getOwner()) {
			getMenuInflater().inflate(R.menu.show_event_edit_context, menu);
			if (aktuellesevent.getCouser(
					aktuellesevent.getCouserIdByPosition((int) ((AdapterContextMenuInfo) menuInfo).id)).getOwner() != 0) {
				menu.findItem(R.id.show_event_edit_context_admin).setVisible(false);
			}
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		userposition = (int) info.id;
		switch (item.getItemId()) {
			case R.id.show_event_edit_context_admin:
				if (aktuellesevent.getCouserIdByPosition(userposition) == benutzer.getId()) {
					aktuellesevent.setOwner(aktuellesevent.getCouserIdByPosition(userposition));
					e_userliste.setAdapter(aktuellesevent.getCouserNameListArrayAdapter(ShowEventEditActivity.this,
							android.R.layout.simple_list_item_1));
				} else
					showDialog(D_ADMIN_WECHSELN);
				return true;
			case R.id.show_event_edit_context_loeschen:
				aktuellesevent.removeCouser(aktuellesevent.getCouserIdByPosition(userposition));
				e_userliste.setAdapter(aktuellesevent.getCouserNameListArrayAdapter(ShowEventEditActivity.this,
						android.R.layout.simple_list_item_1));

				return true;
			default:
				return super.onContextItemSelected(item);
		}

	}

	@SuppressWarnings("deprecation")
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
			case D_ADMIN_WECHSELN:
				AlertDialog.Builder builder = new AlertDialog.Builder(this);

				builder.setTitle(getString(R.string.d_adminwechseln));
				builder.setMessage(getString(R.string.d_adminwechseln_warnung));
				builder.setNegativeButton(R.string.d_nein, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				builder.setPositiveButton(R.string.d_ja, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						aktuellesevent.setOwner(aktuellesevent.getCouserIdByPosition(userposition));
						e_userliste.setAdapter(aktuellesevent.getCouserNameListArrayAdapter(ShowEventEditActivity.this,
								android.R.layout.simple_list_item_1));
					}
				});
				return builder.create();

			case D_NEUER_USER:
				final AutoCompleteTextView neuer_user_email = new AutoCompleteTextView(this);
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
						LinearLayout.LayoutParams.MATCH_PARENT);
				neuer_user_email.setLayoutParams(lp);
				neuer_user_email.setHint(R.string.hint_email);

				ArrayList<String> emailAddressCollection = new ArrayList<String>();
				Cursor emailCur = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
						null, null, null);
				while (emailCur.moveToNext())
					emailAddressCollection.add(emailCur.getString(emailCur
							.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)));
				emailCur.close();
				String[] emailAddresses = new String[emailAddressCollection.size()];
				emailAddressCollection.toArray(emailAddresses);

				ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
						android.R.layout.simple_dropdown_item_1line, emailAddresses);
				neuer_user_email.setAdapter(adapter);

				return new AlertDialog.Builder(this).setTitle(R.string.d_benutzerhinzu).setView(neuer_user_email)
						.setNegativeButton(R.string.d_abbrechen, new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();

							}
						}).setPositiveButton(R.string.d_ok, new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								new LoadSingleUser().execute(neuer_user_email.getText().toString(), "");
								neuer_user_email.setText("");

							}
						}).create();

			case D_EVENT_LOESCHEN:
				return new AlertDialog.Builder(this).setTitle(R.string.d_loeschen).setMessage(R.string.d_sicher)
						.setNegativeButton(R.string.d_abbrechen, new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();

							}
						}).setPositiveButton(R.string.d_ok, new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								delete_event = true;
								new Loader().execute(true);
							}
						}).create();
		}
		return super.onCreateDialog(id);

	}

	protected void speichern() {
		// TODO Auto-generated method stub

		// Fehler suchen
		boolean fehlergefunden = false;
		if (e_titel.getText().toString().length() == 0)
			fehlergefunden = showError(e_titel, getString(R.string.e_bitteangeben));

		// Bill bearbeiten
		if (!fehlergefunden) {
			aktuellesevent.setTitel(e_titel.getText().toString());
			aktuellesevent.setText(e_text.getText().toString());
			aktuellesevent.setSaved(false);

			new Loader().execute(true);

		}

	}

	private boolean showError(EditText ETreferenz, String fehlertext) {
		Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
		ETreferenz.startAnimation(shake);
		ETreferenz.setError(fehlertext);
		ETreferenz.requestFocus();
		return true;
	}

	public class Loader extends AsyncTask<Boolean, String, Boolean> {
		ProgressDialog progDailog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDailog = new ProgressDialog(ShowEventEditActivity.this);
			progDailog.setMessage(getString(R.string.d_laden));
			progDailog.setIndeterminate(false);
			progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDailog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					Loader.this.cancel(true);
					finish();
				}
			});
			progDailog.show();
		}

		protected Boolean doInBackground(Boolean... auchspeichern) {

			// HTTP Client that supports streaming uploads and downloads
			HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, "UTF-8");

			DefaultHttpClient httpclient = new DefaultHttpClient(params);

			List<NameValuePair> httppostpairs = new ArrayList<NameValuePair>();

			// Wenn gespeichert werden soll
			if (auchspeichern[0]) {
				httppostpairs.add(new BasicNameValuePair("e_owner", String.valueOf(aktuellesevent.getOwner())));
				httppostpairs.add(new BasicNameValuePair("e_titel", aktuellesevent.getTitel()));
				httppostpairs.add(new BasicNameValuePair("e_text", aktuellesevent.getText()));
				httppostpairs.add(new BasicNameValuePair("e_currency", aktuellesevent.getLocaleString()));
				httppostpairs.add(new BasicNameValuePair("e_users", aktuellesevent.implodeCousers()));

				// Wenn Event gel�scht werden soll
				if (delete_event) {
					httppostpairs.add(new BasicNameValuePair("delete_event", "1"));
				}
			}

			// Define that I want to use the POST method to grab data from
			// the provided URL
			HttpPost httppost = new HttpPost(StartActivity.DB_CONNECTION_URL + "?action=EventEdit&event_id=" + event_id);

			httppostpairs.add(new BasicNameValuePair("email", benutzer.getEmail()));
			httppostpairs.add(new BasicNameValuePair("pass", benutzer.getPasswort()));

			// Soll ein Bill gel�scht werden?
			// if(delete_user > 0) httppostpairs.add(new
			// BasicNameValuePair("delete_user", String.valueOf(delete_user)));

			try {
				httppost.setEntity(new UrlEncodedFormEntity(httppostpairs));
				// delete_user = 0;
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Web service used is defined
			// httppost.setHeader("Content-type", "application/json");
			httppost.setHeader("Content-type", "application/x-www-form-urlencoded");

			// Used to read data from the URL
			InputStream inputStream = null;

			// Will hold the whole all the data gathered from the URL
			String result = null;

			try {

				// Get a response if any from the web service
				HttpResponse response = httpclient.execute(httppost);

				// The content from the requested URL along with headers, etc.
				HttpEntity entity = response.getEntity();

				// Get the main content from the URL
				inputStream = entity.getContent();

				// JSON is UTF-8 by default
				// BufferedReader reads data from the InputStream until the
				// Buffer is full
				BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);

				// Will store the data
				StringBuilder theStringBuilder = new StringBuilder();

				String line = null;

				// Read in the data from the Buffer untilnothing is left
				while ((line = reader.readLine()) != null) {

					// Add data from the buffer to the StringBuilder
					theStringBuilder.append(line + "\n");
				}

				// Store the complete data in result
				result = theStringBuilder.toString();

			} catch (Exception e) {
				e.printStackTrace();
				return false;
			} finally {

				// Close the InputStream when you're done with it
				try {
					if (inputStream != null)
						inputStream.close();
				} catch (Exception e) {
				}
			}

			JSONObject jsonObject;
			try {
				// Get the root JSONObject
				jsonObject = new JSONObject(result);

				if (!jsonObject.getJSONArray("userdata").isNull(0)) {
					// Benutzer laden
					JSONObject jsonuser = jsonObject.getJSONArray("userdata").getJSONObject(0);
					benutzer.setName(jsonuser.getString("name"));
					benutzer.setId(jsonuser.getInt("id"));

					try {
						// Eventdaten laden
						JSONObject JSONevent = jsonObject.getJSONArray("eventsdata").getJSONObject(0);
						aktuellesevent = new Event(jsonObject.getJSONArray("eventsdata").getJSONObject(0));
						// aktuellesevent.setId((Integer)
						// JSONevent.getInt("id"));
						// aktuellesevent.setTitel(JSONevent.getString("titel"));
						// aktuellesevent.setText(JSONevent.getString("text"));
						// aktuellesevent.setOwner((Integer)
						// JSONevent.getInt("owner"));

						event_id = aktuellesevent.getId();

						JSONArray JSONcousers = jsonObject.getJSONArray("cousersdata");
						for (int i = 0; i < JSONcousers.length(); i++) {
							aktuellesevent.putCouser(new Benutzer(JSONcousers.getJSONObject(i)));
						}

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						aktuellesevent = new Event();
						aktuellesevent.putCouser(benutzer);
						aktuellesevent.setOwner(benutzer.getId());
					}
					aktuellesevent.setSync(true);
					if (auchspeichern[0])
						aktuellesevent.setSaved(true);

				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return true;

		}

		protected void onPostExecute(Boolean result) {
			if (progDailog.isShowing()) {
				progDailog.dismiss();
			}

			if (!result) {
				startActivity((new Intent(getBaseContext(), StartActivity.class))
						.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				return;
			}

			if (benutzer.getId() > 0) {
				benutzer.setSync(true);
				setContentView(R.layout.activity_show_event_edit);
				LayoutInflater layoutInflater = (LayoutInflater) ShowEventEditActivity.this
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				ShowEventEditHeader = (LinearLayout) layoutInflater.inflate(R.layout.header_show_event_edit, null,
						false);

				// Wenn Bill gespechert wurde Toast anzeigen
				if (aktuellesevent.isSaved() && result && !delete_event) {
					Toast.makeText(getBaseContext(), getString(R.string.t_gespeichert), Toast.LENGTH_LONG).show();
					finish();
				}
				if (aktuellesevent.isSaved() && result && delete_event) {
					Toast.makeText(getBaseContext(), getString(R.string.t_geloescht), Toast.LENGTH_LONG).show();
					finish();
				}

				e_userliste = (ListView) findViewById(R.id.show_event_edit_user);
				e_titel = (EditText) ShowEventEditHeader.findViewById(R.id.show_event_edit_titel);
				e_text = (EditText) ShowEventEditHeader.findViewById(R.id.show_event_edit_text);
				Spinner e_currency = (Spinner) ShowEventEditHeader.findViewById(R.id.ShowEventEditCurrency);
				
				ArrayList<Locale> availabeLocales = StartActivity.getAvailableLocales();
				ArrayList<String> loc_str =  new ArrayList<String>();
				for(Locale l : availabeLocales) loc_str.add(NumberFormat.getCurrencyInstance(l).getCurrency().getSymbol()+" ("+l.getDisplayCountry()+")");
				ArrayAdapter<String> e_currency_adapter = new ArrayAdapter<String>(ShowEventEditActivity.this,
						android.R.layout.simple_spinner_item,loc_str);

				e_currency_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				e_currency.setAdapter(e_currency_adapter);
				int i = 0;
				int current_index = 0;
				Locale current_string = aktuellesevent.getLocale();
				for (Locale c : availabeLocales) {
					if (c.equals(current_string))
						current_index = i;
					i++;
				}
				e_currency.setSelection(current_index);
				e_currency.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {

						aktuellesevent.setLocale(StartActivity.getAvailableLocales().get(position));
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});

				if (aktuellesevent.isLoaded() && aktuellesevent.getOwner() == benutzer.getId() && !delete_event) {
					setTitle(aktuellesevent.getTitel());

					e_titel.setText(aktuellesevent.getTitel());
					e_text.setText(aktuellesevent.getText());

					e_userliste.addHeaderView(ShowEventEditHeader, null, false);
					e_userliste.setAdapter(aktuellesevent.getCouserNameListArrayAdapter(ShowEventEditActivity.this,
							android.R.layout.simple_list_item_1));
					e_userliste.setClickable(true);
					registerForContextMenu(e_userliste);
					ShowEventEditHeader.findViewById(R.id.show_event_edit_adduser).setOnClickListener(
							new OnClickListener() {

								@Override
								public void onClick(View v) {
									showDialog(D_NEUER_USER);

								}
							});
					ShowEventEditHeader.findViewById(R.id.ShowEventEditUserManagerButton).setOnClickListener(
							new OnClickListener() {
								@Override
								public void onClick(View v) {
									startActivityForResult(new Intent(getBaseContext(), ShowUserManagerActivity.class)
											.putExtra("REQUESTCODE", ShowUserManagerActivity.REQUESTCODE_USER_MANAGER),
											ShowUserManagerActivity.REQUESTCODE_USER_MANAGER);
								}
							});
				} else
					startActivity(new Intent(getBaseContext(), ShowEventActivity.class));
			} else {
				// Wenn Benutzer nicht verifiziert ist dann zur�ck
				Intent intent = new Intent(getBaseContext(), StartActivity.class);
				startActivity(intent);
			}

		}

	}

	@Override
	protected void onActivityResult(int aRequestCode, int aResultCode, Intent aData) {
		switch (aRequestCode) {
			case ShowUserManagerActivity.REQUESTCODE_USER_MANAGER:
				if (aResultCode == Activity.RESULT_OK) {
					new LoadSingleUser().execute("", aData.getIntExtra("selected_user_id", 0) + "");
				}
				break;
		}
		super.onActivityResult(aRequestCode, aResultCode, aData);
	}

	public class LoadSingleUser extends AsyncTask<String, String, Boolean> {
		ProgressDialog progDailog;
		Benutzer new_user = new MeineDaten(null, null);

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDailog = new ProgressDialog(ShowEventEditActivity.this);
			progDailog.setMessage(getString(R.string.d_laden));
			progDailog.setIndeterminate(false);
			progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDailog.setCancelable(false);
			progDailog.show();
		}

		protected Boolean doInBackground(String... neuer_user_email) {

			// HTTP Client that supports streaming uploads and downloads
			HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, "UTF-8");

			DefaultHttpClient httpclient = new DefaultHttpClient(params);

			List<NameValuePair> httppostpairs = new ArrayList<NameValuePair>();

			// Define that I want to use the POST method to grab data from
			// the provided URL
			HttpPost httppost = new HttpPost(StartActivity.DB_CONNECTION_URL + "?action=loaduser&user_email="
					+ neuer_user_email[0] + "&selected_user_id=" + neuer_user_email[1]);

			httppostpairs.add(new BasicNameValuePair("email", benutzer.getEmail()));
			httppostpairs.add(new BasicNameValuePair("pass", benutzer.getPasswort()));

			try {
				httppost.setEntity(new UrlEncodedFormEntity(httppostpairs));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			httppost.setHeader("Content-type", "application/x-www-form-urlencoded");
			InputStream inputStream = null;
			String result = null;

			try {
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();
				inputStream = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
				StringBuilder theStringBuilder = new StringBuilder();

				String line = null;
				while ((line = reader.readLine()) != null)
					theStringBuilder.append(line + "\n");
				result = theStringBuilder.toString();
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			} finally {
				try {
					if (inputStream != null)
						inputStream.close();
				} catch (Exception e) {
				}
			}
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(result);

				if (jsonObject.getJSONArray("loaduserdata").length() == 0) {
					new_user = null;
				} else {
					new_user = new Benutzer(jsonObject.getJSONArray("loaduserdata").getJSONObject(0));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return true;
		}

		protected void onPostExecute(Boolean result) {
			if (progDailog.isShowing()) {
				progDailog.dismiss();
			}

			if (!result) {
				startActivity((new Intent(getBaseContext(), StartActivity.class))
						.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				return;
			} else {
				if (new_user == null) {
					Toast.makeText(getApplicationContext(), getString(R.string.t_keinusermitdieseremail),
							Toast.LENGTH_LONG).show();
				} else {
					if (aktuellesevent.getCouser(new_user.getId()) == null) {
						aktuellesevent.putCouser(new_user);
						e_userliste.setAdapter(aktuellesevent.getCouserNameListArrayAdapter(ShowEventEditActivity.this,
								android.R.layout.simple_list_item_1));
					} else
						Toast.makeText(getApplicationContext(), getString(R.string.t_userschonvorhanden),
								Toast.LENGTH_LONG).show();
				}
			}
		}

	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.
	}

}
