package com.aaronbb.fairculator;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aaronbb.fairculator.SQLite.FairculatorDB;
import com.aaronbb.fairculator.daten.Event;
import com.aaronbb.fairculator.daten.HttpAnfrage;
import com.aaronbb.fairculator.daten.MeineDaten;
import com.aaronbb.fairculator.extra.ChangeLog;

public class StartActivity extends FairculatorActivity {

	private Menu menu;
	EditText et_signin_email = null;
	EditText et_signin_pass = null;
	EditText et_resetpass_email;

	private static final int SIGN_IN_DIALOG = 0;
	private static final int ERROR_NO_CONNECTION = -1;
	private static final int RESET_PASSWORD = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ShortcutBeimErstenMalInstallieren();

		// Abk�rzung
		// Wenn event oder bill per Intent gesendet dann dieses bevorzugen
		Bundle sc_extras = this.getIntent().getExtras();
		if (sc_extras != null) {
			if (sc_extras.containsKey("shortcut")) {
				if (sc_extras.getString("shortcut").equals("new_bill")) {
					sharedprefs.edit().putInt("event_id", sc_extras.getInt("event_id", 0)).commit();
					sharedprefs.edit().putInt("bill_id", sc_extras.getInt("bill_id", 0)).commit();
					startActivity(new Intent(getBaseContext(), ShowBillEditActivity.class));
				}
			}
		}

		// Dialogformularfeld initialisieren
		et_resetpass_email = new EditText(this);

		Log.v("QUERY", FairculatorDB.Users.SQL_CREATE_TABLE);
		Log.v("QUERY", FairculatorDB.Events.SQL_CREATE_TABLE);
		Log.v("QUERY", FairculatorDB.Bills.SQL_CREATE_TABLE);

	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		this.menu = menu;
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.start, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_settings:
				Intent intent = new Intent(getBaseContext(), ShowSettingsActivity.class);
				startActivity(intent);
				return true;
			case R.id.start_abmelden:
				SharedPreferences.Editor prefEditor = sharedprefs.edit();
				prefEditor.remove("email");
				prefEditor.remove("pass");
				prefEditor.commit();
				benutzer.abmelden();
				Intent i = new Intent(getBaseContext(), StartActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
				return true;
			case R.id.start_new_event:
				sharedprefs.edit().putInt("event_id", 0).commit();
				intent = new Intent(getBaseContext(), ShowEventEditActivity.class);
				startActivity(intent);
				return true;

			case R.id.show_user_edit:
				intent = new Intent(getBaseContext(), ShowUserEditActivity.class);
				startActivity(intent);
				return true;
		}

		return super.onOptionsItemSelected(item);
	}


	@Override
	protected void onResume() {
		super.onResume();
		new Loader().execute();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
			case ERROR_NO_CONNECTION:

				AlertDialog.Builder ebuilder = new AlertDialog.Builder(this);
				ebuilder.setTitle(getString(R.string.d_fehler));
				ebuilder.setMessage(getString(R.string.d_keineverbindung));
				ebuilder.setCancelable(true);
				ebuilder.setNegativeButton(R.string.d_beenden, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						startActivity(new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME).setFlags(
								Intent.FLAG_ACTIVITY_NEW_TASK));

					}
				});
				ebuilder.setPositiveButton(R.string.d_neuladen, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						new Loader().execute();
					}
				});
				return ebuilder.create();

			case SIGN_IN_DIALOG:

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				LayoutInflater inflater = this.getLayoutInflater();
				View layout = inflater.inflate(R.layout.dialog_login, null);
				builder.setView(layout);
				builder.setTitle(getString(R.string.d_anmelden));

				// get the References of views
				et_signin_email = (EditText) layout.findViewById(R.id.editTextEmailToLogin);
				et_signin_pass = (EditText) layout.findViewById(R.id.editTextPasswordToLogin);

				builder.setPositiveButton(R.string.d_anmelden, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						benutzer.setEmail(et_signin_email.getText().toString());
						benutzer.setPasswort(et_signin_pass.getText().toString());

						dialog.dismiss();

						new Loader().execute();
					}
				});
				builder.setNeutralButton(R.string.d_passwortvergessen, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						showDialog(RESET_PASSWORD);
						dialog.dismiss();
					}
				});
				builder.setNegativeButton(R.string.d_newuser, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						startActivity(new Intent(getBaseContext(), ShowUserEditActivity.class));
						dialog.dismiss();
					}
				});
				builder.setCancelable(false);
				return builder.create();
			case RESET_PASSWORD:
				et_resetpass_email.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
				et_resetpass_email.setSelectAllOnFocus(true);
				et_resetpass_email.setHint(R.string.hint_email);
				final AlertDialog d_resetpassword = new AlertDialog.Builder(this).setTitle(R.string.hint_email)
						.setView(et_resetpass_email).setPositiveButton(R.string.d_neuespasswortanfordern, null)
						.setNegativeButton(R.string.d_abbrechen, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								showDialog(SIGN_IN_DIALOG);
							}
						}).create();
				d_resetpassword.setOnShowListener(new DialogInterface.OnShowListener() {
					@Override
					public void onShow(DialogInterface dialog) {
						Button b = d_resetpassword.getButton(AlertDialog.BUTTON_POSITIVE);
						b.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View view) {
								if (!android.util.Patterns.EMAIL_ADDRESS.matcher(
										et_resetpass_email.getText().toString()).matches()) {
									showError(et_resetpass_email, getString(R.string.e_keineemailadresse));
								} else {
									if (et_signin_email != null)
										et_signin_email.setText(et_resetpass_email.getText().toString());
									benutzer.setEmail(et_resetpass_email.getText().toString());
									new Loader().execute("resetpassword");
									Toast.makeText(getBaseContext(), getString(R.string.t_neuespasswortperemail),
											Toast.LENGTH_LONG).show();
									d_resetpassword.dismiss();
								}
							}
						});
					}
				});
				d_resetpassword.setCancelable(false);
				return d_resetpassword;
		}
		return null;
	}

	@Override
	public void onCreateView() {
		super.onCreateView();
		
		setContentView(R.layout.activity_start);

		try {
			MenuItem MiAbmelden = menu.findItem(R.id.start_abmelden);
			MiAbmelden.setVisible(true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		((TextView) findViewById(R.id.StartHeader)).setText(getString(R.string.text_einkaufslistenvonundfuer)
				+ " " + benutzer.getName());
		ListView list = (ListView) findViewById(R.id.StartListview);

		list.setAdapter(new ListAdapter(StartActivity.this));
		list.setClickable(true);
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {

				Intent intent = new Intent(getBaseContext(), ShowEventActivity.class);
				sharedprefs.edit().putInt("event_id", (Integer) parent.getAdapter().getItem(position)).commit();
				startActivity(intent);
			}

		});
	}

	public class Loader extends AsyncTask<String, String, Boolean> {
		ProgressDialog progDailog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDailog = new ProgressDialog(StartActivity.this);
			progDailog.setMessage(getString(R.string.d_laden));
			progDailog.setIndeterminate(false);
			progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDailog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					Loader.this.cancel(true);
					finish();
				}
			});
			progDailog.show();
		}

		protected Boolean doInBackground(String... arg0) {
			String result = null;
			if(benutzer.getEmail()!=null) result = executeRequest(
					new HttpAnfrage(
							DB_CONNECTION_URL + "?action=loadevents&email=" 
					+ benutzer.getEmail().toLowerCase() + "&pass="
					+ benutzer.getPasswort()));
			if(result==null) return false;

			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(result);

				if (!jsonObject.getJSONArray("userdata").isNull(0)) {
					benutzer.update(new MeineDaten(jsonObject.getJSONArray("userdata")
							.getJSONObject(0)));
					benutzer.setSync(benutzer.getId() > 0);
					
					JSONArray queryArray = jsonObject.getJSONArray("eventsdata");

					//benutzer.clearEvents();
					for (int i = 0; i < queryArray.length(); i++) {
						JSONObject obj = queryArray.getJSONObject(i);
						Event aktuellerechnung;
						if(benutzer.hasEvent(obj.getInt("id"))){
							aktuellerechnung = benutzer.getEvent(obj.getInt("id"));
						}else aktuellerechnung = new Event();

						aktuellerechnung.setId((Integer) obj.getInt("id"));
						aktuellerechnung.setAnzahlAnRechnungen((Integer) obj.getInt("COUNT(*)"));
						aktuellerechnung.setTitel(obj.getString("titel"));
						aktuellerechnung.setText(obj.getString("text"));
						benutzer.putEvent(aktuellerechnung);
					}

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.v("res", result);
				e.printStackTrace();
				// return false;
			}

			return true;

		}

		protected void onPostExecute(Boolean result) {
			if (progDailog.isShowing()) {
				progDailog.dismiss();
			}

			if (!result) {
				showDialog(ERROR_NO_CONNECTION);
			}

			if (benutzer.isVerified()) {
				// Changelog abfragen
				ChangeLog cl = new ChangeLog(StartActivity.this);
				if (cl.firstRun() && !cl.getLogDialog().isShowing())
					cl.getLogDialog().show();

				onCreateView();

			} else {
				showDialog(SIGN_IN_DIALOG);
			}

		}

	}

	private boolean showError(EditText ETreferenz, String fehlertext) {
		Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
		ETreferenz.startAnimation(shake);
		ETreferenz.setError(fehlertext);
		ETreferenz.requestFocus();
		return true;
	}

	private void ShortcutBeimErstenMalInstallieren() {
		// Adding shortcut for MainActivity
		// on Home screen
		if (sharedprefs.getBoolean("ersterStart", true)) {
			Intent shortcutIntent = new Intent(getApplicationContext(), StartActivity.class);

			shortcutIntent.setAction(Intent.ACTION_MAIN);

			Intent addIntent = new Intent();
			addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
			addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
			addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
					Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.drawable.ic_launcher));

			addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
			getApplicationContext().sendBroadcast(addIntent);
			sharedprefs.edit().putBoolean("ersterStart", false).commit();
		}
	}

	public static class ListAdapter extends BaseAdapter {

		private final Activity activity;
		private static LayoutInflater inflater = null;
		private ArrayList<Integer> keyTable;

		public ListAdapter(Activity a) {
			activity = a;
			inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			keyTable = benutzer.getPositionKeyTable();
		}

		public int keyAt(int position) {
			return keyTable.get(position);
		}

		@Override
		public boolean areAllItemsEnabled() {
			return true;
		}

		@Override
		public boolean isEnabled(int arg0) {
			return true;
		}

		@Override
		public int getCount() {
			return StartActivity.benutzer.getAnzahlAnEvents();
		}

		@Override
		public Object getItem(int position) {
			return keyAt(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			int event_id = keyAt(position);
			View vi = inflater.inflate(R.layout.list_events_row, null);

			((TextView) vi.findViewById(R.id.StartEventListTitle)).setText(benutzer.getEvent(event_id).getTitel());
			((TextView) vi.findViewById(R.id.StartEventListSubtitle)).setText(benutzer.getEvent(event_id).getText());
			((TextView) vi.findViewById(R.id.StartEventListBillCount)).setText(String.valueOf(benutzer.getEvent(
					event_id).getAnzahlAnRechungen()));

			return vi;
		}
	}


}
