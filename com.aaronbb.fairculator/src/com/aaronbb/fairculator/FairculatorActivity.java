package com.aaronbb.fairculator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.aaronbb.fairculator.daten.Benutzer;
import com.aaronbb.fairculator.daten.Bill;
import com.aaronbb.fairculator.daten.Event;
import com.aaronbb.fairculator.daten.HttpAnfrage;
import com.aaronbb.fairculator.daten.MeineDaten;
import com.aaronbb.fairculator.daten.SyncRequest;
import com.google.analytics.tracking.android.EasyTracker;

public class FairculatorActivity extends Activity {
	protected static MeineDaten benutzer = null;
	protected SharedPreferences sharedprefs;
	protected SharedPreferences einstellungen;
	private String cacheFile = "cache.dat";
	private File cf;

	// public static final String BASE_URL =
	// "http://192.168.178.23/fairculator/";
	// public static final String BASE_URL =
	// "http://www.duimwap.de/fairculator/";
	public static final String BASE_URL = "http://duimwap.de/fairculator/";
	public static final String DB_CONNECTION_URL = BASE_URL + "fairculator.php";
	public static final String LOAD_BILL_PIC_URL = BASE_URL + "loadbillpicture.php";
	public static final int FARBE_WALD_GRUEN = Color.parseColor("#216700");
	public static final int FARBE_WALD_ROT = Color.parseColor("#600924");

	public static ArrayList<Locale> getAvailableLocales() {
		String[] locale_strings = { "en_US", "de_DE", "zh_CN", "zh_TW", "cs_CZ", "nl_BE", "nl_NL", "en_AU", "en_GB",
				"en_CA", "en_NZ", "en_SG", "fr_BE", "fr_CA", "fr_FR", "fr_CH", "de_AT", "de_LI", "de_CH", "it_IT",
				"it_CH", "ja_JP", "ko_KR", "pl_PL", "ru_RU", "es_ES", "ar_EG", "ar_IL", "bg_BG", "ca_ES", "hr_HR",
				"da_DK", "en_IN", "en_IE", "en_ZA", "fi_FI", "el_GR", "iw_IL", "hi_IN", "hu_HU", "in_ID", "lv_LV",
				"lt_LT", "nb_NO", "pt_BR", "pt_PT", "ro_RO", "sr_RS", "sk_SK", "sl_SI", "es_US", "sv_SE", "tl_PH",
				"th_TH", "tr_TR", "uk_UA", "vi_VN" };
		ArrayList<Locale> locs = new ArrayList<Locale>();
		for (String l : locale_strings) {
			String[] loc_str_parts = l.split("_");
			Locale lo = new Locale(loc_str_parts[0], loc_str_parts[1]);
			locs.add(lo);
		}
		return locs;
	}

	public static NumberFormat GeldFormat(Locale l) {
		return NumberFormat.getCurrencyInstance(l);
	}

	public static NumberFormat GeldFormat() {
		return NumberFormat.getCurrencyInstance();
	}

	public static String thousandSeparator = ((DecimalFormat) NumberFormat.getInstance()).format(1234.5)
			.substring(1, 2);
	public static String decimalSeparator = ((DecimalFormat) NumberFormat.getInstance()).format(1234.5).substring(5, 6);
	public static Format dateFormat = DateFormat.getDateInstance();

	// public static DecimalFormat GeldFormatOhneE() {
	// DecimalFormat GeldFormatOhneE = (DecimalFormat)
	// NumberFormat.getInstance();
	// // Geldformat einstellen
	// GeldFormatOhneE.setMaximumFractionDigits(2);
	// GeldFormatOhneE.setMinimumFractionDigits(2);
	// return GeldFormatOhneE;
	// }

	public static DecimalFormat GeldFormatOhneE(Locale l) {
		DecimalFormat GeldFormatOhneE = (DecimalFormat) NumberFormat.getInstance(l);
		// Geldformat einstellen
		GeldFormatOhneE.setMaximumFractionDigits(GeldFormat(l).getMaximumFractionDigits());
		GeldFormatOhneE.setMinimumFractionDigits(GeldFormat(l).getMinimumFractionDigits());
		return GeldFormatOhneE;
	}

	public static Double parseMoney(String string, Locale l) {
		// String falscherdezimalseperator = ".";
		// string = string.replaceAll("\\" + falscherdezimalseperator,
		// StartActivity.decimalSeparator);

		String[] teile = string.split("\\+");
		Double gesamt = 0.0;
		Boolean doublerkannt = false;
		if (string.equals(""))
			doublerkannt = true;
		for (String t : teile) {
			Double t_double = 0.0;
			try {
				t_double = StartActivity.GeldFormatOhneE(l).parse(t.replace("(-?\\d+((\\.|,)\\d+)?)", "$1"))
						.doubleValue();
				doublerkannt = true;
				gesamt += t_double;
			} catch (ParseException e1) {
			}
		}
		if (doublerkannt) {
			return gesamt;
		} else
			return null;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		// Daten laden
		sharedprefs = getSharedPreferences("fairculator", MODE_PRIVATE);
		// Einstellungen laden
		einstellungen = PreferenceManager.getDefaultSharedPreferences(this);

		cf = new File(this.getFilesDir(), cacheFile);


        // Create the dummy account
        mAccount = CreateSyncAccount(this);

	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		if (savedInstanceState != null) {
			benutzer = savedInstanceState.containsKey("benutzer") ? (MeineDaten) savedInstanceState
					.getSerializable("benutzer") : null;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (benutzer == null) {

			try {
				Log.v("Load Size", cf.length() + "");
				FileInputStream fis = new FileInputStream(cf);
				// fis = new FileInputStream(cf);
				ObjectInputStream is = new ObjectInputStream(fis);
				benutzer = (MeineDaten) is.readObject();
				is.close();
				fis.close();
			} catch (Exception e) {
				e.printStackTrace();
				// leeren Benutzer erzeugen
				benutzer = new MeineDaten();
			}
		}

		// Automatisch nach dem Laden den Screen erzeugen
		if (benutzer != null)
			onCreateView();
	}

	// View erzeugen
	public void onCreateView() {

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (benutzer != null)
			outState.putSerializable("benutzer", benutzer);
	}

	public void onPause() {
		super.onPause();

		// Benutzerdaten speichern
		if (benutzer != null && benutzer.isVerified()/*
													 * &&
													 * benutzer.getEmail()!=null
													 * && (benutzer.getEmail()
													 * !=
													 * sharedprefs.getString("email"
													 * , ""))
													 */) {
			// SharedPreferences.Editor prefEditor = sharedprefs.edit();
			// prefEditor.putString("email", benutzer.getEmail());
			// prefEditor.putString("pass", benutzer.getPasswort());
			// prefEditor.commit();

			try {
				FileOutputStream fos = new FileOutputStream(cf);
				ObjectOutputStream os = new ObjectOutputStream(fos);
				os.writeObject(benutzer);
				os.close();
				fos.close();
				Log.v("Save Size", cf.length() + "");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.
	}

	public String executeRequest(HttpAnfrage new_httppost) {
		benutzer.addHttpAnfrage(new_httppost);

		LinkedList<HttpAnfrage> neue_liste = new LinkedList<HttpAnfrage>();

		HttpAnfrage httpanfrage;
		// Will hold the whole all the data gathered from the URL
		String result = null;
		while (benutzer.hasHttpAnfrage()) {
			httpanfrage = benutzer.getFirstHttpAnfrage();

			// k�nnte null sein wenn sie gel�scht wurde
			if (httpanfrage.getUrl() != null) {

				HttpPost httppost = new HttpPost(httpanfrage.getUrl());
				if (httpanfrage.getPost_vars() != null) {
					// set up post data
					ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
					Iterator<String> it = httpanfrage.getPost_vars().keySet().iterator();
					while (it.hasNext()) {
						String key = it.next();
						nameValuePairs.add(new BasicNameValuePair(key, httpanfrage.getPost_vars().get(key)));
					}
					try {
						httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				// HTTP Client that supports streaming uploads and downloads
				DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());

				// Web service used is defined
				// httppost.setHeader("Content-type", "application/json");
				httppost.setHeader("Content-type", "application/x-www-form-urlencoded");

				// Used to read data from the URL
				InputStream inputStream = null;

				try {

					// Get a response if any from the web service
					HttpResponse response = httpclient.execute(httppost);

					// The content from the requested URL along with headers,
					// etc.
					HttpEntity entity = response.getEntity();

					// Get the main content from the URL
					inputStream = entity.getContent();

					// JSON is UTF-8 by default
					// BufferedReader reads data from the InputStream until the
					// Buffer is full
					BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);

					// Will store the data
					StringBuilder theStringBuilder = new StringBuilder();

					String line = null;

					// Read in the data from the Buffer untilnothing is left
					while ((line = reader.readLine()) != null) {

						// Add data from the buffer to the StringBuilder
						theStringBuilder.append(line + "\n");
					}

					// Store the complete data in result
					result = theStringBuilder.toString();

				} catch (Exception e) {
					e.printStackTrace();
					// return false;
				} finally {

					// Close the InputStream when you're done with it
					try {
						if (inputStream != null)
							inputStream.close();
					} catch (Exception e) {
					}
				}
				Log.v("Post ausgef�hrt", httppost.getURI().toString());

				if (result == null && httpanfrage.getPost_vars() != null)
					neue_liste.add(httpanfrage);
			}
		}

		// neue_liste in die Liste des benutzers �bertragen
		for (HttpAnfrage hp : neue_liste)
			benutzer.addHttpAnfrage(hp);

		return result;
	}

	public boolean handleSyncRequest(SyncRequest request) {
		String result = null;

		// wenn Bedarf zur Synchronisation besteht

		HttpPost httppost = new HttpPost(DB_CONNECTION_URL + "?" + request.getQuery() + "&email="
				+ benutzer.getEmail().toLowerCase() + "&pass=" + benutzer.getPasswort());

		// Wenn was ge�ndert wurde
		if (!request.isSync()) {
			// set up post data
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			Iterator<String> it = request.getPostVars().keySet().iterator();
			while (it.hasNext()) {
				String key = it.next();
				nameValuePairs.add(new BasicNameValuePair(key, request.getPostVars().get(key)));
			}
			try {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// HTTP Client that supports streaming uploads and downloads
		DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());

		// Web service used is defined
		// httppost.setHeader("Content-type", "application/json");
		httppost.setHeader("Content-type", "application/x-www-form-urlencoded");

		// Used to read data from the URL
		InputStream inputStream = null;

		try {

			// Get a response if any from the web service
			HttpResponse response = httpclient.execute(httppost);

			// The content from the requested URL along with headers,
			// etc.
			HttpEntity entity = response.getEntity();

			// Get the main content from the URL
			inputStream = entity.getContent();

			// JSON is UTF-8 by default
			// BufferedReader reads data from the InputStream until the
			// Buffer is full
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);

			// Will store the data
			StringBuilder theStringBuilder = new StringBuilder();

			String line = null;

			// Read in the data from the Buffer untilnothing is left
			while ((line = reader.readLine()) != null) {

				// Add data from the buffer to the StringBuilder
				theStringBuilder.append(line + "\n");
			}

			// Store the complete data in result
			result = theStringBuilder.toString();

		} catch (Exception e) {
			e.printStackTrace();
			// return false;
		} finally {

			// Close the InputStream when you're done with it
			try {
				if (inputStream != null)
					inputStream.close();
			} catch (Exception e) {
			}
		}

		// Wenn keine Verbundung hergestellt werden konnte, false zur�ckgeben
		if (result == null)
			return false;

		try {
			JSONObject jsonObject = new JSONObject(result);
			if (!jsonObject.getJSONArray("userdata").isNull(0)) {
				// Benutzer laden
				benutzer.update(new MeineDaten(jsonObject.getJSONArray("userdata").getJSONObject(0)));
				benutzer.setSync(benutzer.getId() > 0);

				// Wenn ein Bill geladen wurde
				if (request instanceof Bill) {
					// Event laden
					if (!jsonObject.isNull("eventsdata")) {
						Event geladenes_event = new Event(jsonObject.getJSONArray("eventsdata").getJSONObject(0));
						benutzer.putEvent(geladenes_event);

						Event aktuelles_event = benutzer.getEvent(geladenes_event.getId());

						// Bill laden
						if (!jsonObject.isNull("billdata")) {

							Bill gerade_geladener_bill;

							gerade_geladener_bill = new Bill(jsonObject.getJSONArray("billdata").getJSONObject(0),
									aktuelles_event);

							// andere Usernamen laden damit diese den ids
							// zugeordnet
							// werden k�nnen
							if (!jsonObject.isNull("cousersdata")) {
								aktuelles_event.clearCouser();

								JSONArray JSONcousers = jsonObject.getJSONArray("cousersdata");
								for (int i = 0; i < JSONcousers.length(); i++) {
									Benutzer geladener_couser = new Benutzer(JSONcousers.getJSONObject(i));

									// Wenn der Benutzer bereits lokal
									// gespeichert
									// ist,
									// ihn nicht neu erzeugen sondern seine
									// Daten
									// updaten und ihn verwenden
									benutzer.putCouser(geladener_couser);
									aktuelles_event.putCouser(benutzer.getCouser(geladener_couser.getId()));
								}
							}

							// Shops laden damit diese den ids zugeordnet werden
							// k�nnen
							JSONArray JSONshops = jsonObject.getJSONArray("shopsdata");
							benutzer.clearShops();
							for (int i = 0; i < JSONshops.length(); i++) {
								benutzer.addShop(JSONshops.getJSONObject(i).getString("shop"));
							}

							// Wenn ein neuer Billgespeichert wurde, die ID auch
							// in
							// den
							// prefs speichern, zb f�rs Bild
							sharedprefs.edit().putInt("bill_id", gerade_geladener_bill.getId()).commit();

							// die geladene Id wird an den Request zur�ckgegeben
							// falls ein neuer Bill gespeichert wurde und auch
							// im Event die neue Id speichern
							if (request.getId() <= 0) {
								aktuelles_event.delBill(request.getId());
								request.setId(gerade_geladener_bill.getId());
							}
							aktuelles_event.putBill(gerade_geladener_bill);
						}

					}
				}

				// Wenn ein Event geladen wurde
				if (request instanceof Event) {

				}

				// Wenn ein alle Events geladen wurden
				if (request instanceof MeineDaten) {

				}

			} else
				return false;
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return true;
	}

	protected void toast(String s) {
		Toast.makeText(getBaseContext(), s, Toast.LENGTH_LONG).show();
	}

    // Constants
    // The authority for the sync adapter's content provider
    public static final String AUTHORITY = "com.aaronbb.fairculator.Sync.provider";
    // An account type, in the form of a domain name
    public static final String ACCOUNT_TYPE = "fairculator.aaronbb.com";
    // The account name
    public static final String ACCOUNT = "dummyaccount";
    // Instance fields
    Account mAccount;

    
	/**
     * Create a new dummy account for the sync adapter
     *
     * @param context The application context
     */
	
    public static Account CreateSyncAccount(Context context) {
        // Create the account type and default account
        Account newAccount = new Account(
                ACCOUNT, ACCOUNT_TYPE);
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call context.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
        } else {
            /*
             * The account exists or some other error occurred. Log this, report it,
             * or handle it internally.
             */
        }
        return newAccount;
    }

}
