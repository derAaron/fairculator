#!/usr/bin/ruby

def increment(filename)
    # Read it
    file_str = ''
    if File.exists?(filename)
        file_str = File.open(filename, 'r') {|file| file.read }
    end 

    parts = file_str.match("android:versionCode=\"(.*)\"")
    text = parts[0]
    version = parts[1]
    newversion = (version.to_i + 1).to_s

    text = text.gsub(version, newversion)
    file_str = file_str.gsub(parts[0], text)

    # Write
    File.open(filename, 'w') {|file| file.write(file_str) }
    puts "incrementing from " + version + " to " + newversion
end

if ARGV.length < 1
    puts 'Usage: VersionIncrement '
else
    increment(ARGV.first)
end